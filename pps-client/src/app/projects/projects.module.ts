import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleProjectComponent } from './single-project/single-project.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { ListProjectsComponent } from './list-projects/list-projects.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ProjectsService } from '../core/services/projects.service';
import { ProjectDataComponent } from './single-project/project-data/project-data.component';
import { ProjectDetailsComponent } from './single-project/project-details/project-details.component';

/* const routes: Routes = [
  { path: 'projects', component: ListProjectsComponent },
  { path: 'projects/create', component: CreateProjectComponent},
  { path: 'projects/:id', component: SingleProjectComponent },
]; */

@NgModule({
  declarations: [
    SingleProjectComponent,
    CreateProjectComponent,
    ListProjectsComponent,
    ProjectDataComponent,
    ProjectDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    // RouterModule.forChild(routes),
  ],
  providers: [
    ProjectsService,
  ],
  exports: [
    RouterModule
  ]
})
export class ProjectsModule { }
