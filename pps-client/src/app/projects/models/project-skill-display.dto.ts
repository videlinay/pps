export class ProjectSkillDisplayDTO {
    public skillName: string;
    public totalHours: number;
}