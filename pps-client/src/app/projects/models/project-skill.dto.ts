export class ProjectSkillDTO {
    public id: number;
    public skillName: string;
    public totalHours: number;
    public hoursWorked: number;
}
