export class ProjectEmployeeDTO {
    public id: number;
    public employeeName: string;
    public skillName: string;
    public dailyTimeInput: number;
}
