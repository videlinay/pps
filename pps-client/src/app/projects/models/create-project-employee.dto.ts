export class CreateProjectEmployeeDTO {
    public skillId: number;
    public employeeId: number;
    public dailyTimeInput: number;
}
