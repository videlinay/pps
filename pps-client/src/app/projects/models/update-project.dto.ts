export class UpdateProjectDTO {
    public name?: string;
    public description?: string;
    public managementTotalHours?: number;
}
