export class UpdateProjectSkillDTO {
    public projectSkillId: number;
    public totalHours: number;
}
