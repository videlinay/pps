export class StartProjectDTO {
    public name: string;
    public description: string;
    public targetDays: number;
    public managementTotalHours: number;
    public managementDailyHours: number;
}
