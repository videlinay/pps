import { Component, OnInit, ViewChild } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { ProjectDTO } from '../models/project.dto';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/common/userDTOs/user';
import { UsersService } from 'src/app/core/services/user.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatCheckbox } from '@angular/material/checkbox';

@Component({
  selector: 'app-list-projects',
  templateUrl: './list-projects.component.html',
  styleUrls: ['./list-projects.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListProjectsComponent implements OnInit {
  public columnsToDisplay = ['id', 'name', 'status', 'reporter', 'targetDays'];
  public expandedProject: ProjectDTO | null;
  public loggedUserId: number;
  public loggedUser: User;
  public projectId: number;
  public project: ProjectDTO;
  public projects: ProjectDTO[];
  public selectedProject: ProjectDTO;
  public filteredProjects: ProjectDTO[];
  public workingHoursLeft: number;

  @ViewChild('checkBox') public checkBox: MatCheckbox;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(
    private readonly projectsService: ProjectsService,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly router: Router,

    // private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {

    this.projectsService.getAllProjects()
    .subscribe((data: ProjectDTO[]) => {
      this.projects = data;
      this.filteredProjects = data;

    });

    this.authService.loggedUser$.subscribe((user) => {
      this.loggedUserId = +user.id;
      this.workingHoursLeft = +user.workingHoursLeft;
    });
}

  public createProject() {
    this.router.navigate(['/projects/create']);
  }

  public change(checkBox: MatCheckbox) {
    if (checkBox.checked) {
      this.usersService.getProjects(this.loggedUserId)
      .subscribe((data: ProjectDTO[]) => this.filteredProjects = data);
    }else if (!checkBox.checked){
      this.projectsService.getAllProjects()
      .subscribe((data: ProjectDTO[]) => this.filteredProjects = data);
    }
  }
  public onSelect(project: ProjectDTO): void {
    this.selectedProject = project;
    this.router.navigate(['/projects', project.id]);
  }

  public applyFilterProjectName(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (!filterValue) {
      this.filteredProjects = this.projects;
    } else {
      this.filteredProjects = this.projects.filter(project => project.name.toLowerCase().includes(filterValue.toLowerCase()));
    }
  }

  public applyFilterStatus(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (!filterValue) {
      this.filteredProjects = this.projects;
    } else {
      this.filteredProjects = this.projects.filter(project => project.status.toLowerCase().includes(filterValue.toLowerCase()));
    }
  }

  public applyFilterManager(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (!filterValue) {
      this.filteredProjects = this.projects;
    } else {
      this.filteredProjects = this.projects.filter(project => project.reporter.toLowerCase().includes(filterValue.toLowerCase()));
    }
  }

  /*   public selectProject(id: number): void {
    this.projectId = id;
    this.router.navigate(['/projects', id]);
  } */
}
