import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectDTO } from '../../models/project.dto';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectsService } from '../../../core/services/projects.service';
import { AuthService } from '../../../core/services/auth.service';
import { User } from '../../../common/userDTOs/user';
import { Subscription } from 'rxjs';
import { NotificatorService } from '../../../core/services/notificator.service';
import { UpdateProjectDTO } from '../../models/update-project.dto';
import { UpdateProjectManagerInputDTO } from '../../models/update-manager-input.dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AgreeModalComponent } from 'src/app/shared/components/agree-modal/agree-modal.component';


@Component({
  selector: 'app-project-data',
  templateUrl: './project-data.component.html',
  styleUrls: ['./project-data.component.css']
})
export class ProjectDataComponent implements OnInit, OnDestroy {
  public project: ProjectDTO;
  public projectId: number;
  private loggedUserSubscription: Subscription;
  public loggedUser: User;
  public loggedUserName: string;
  public withinTarget: string;
  public projectEditForm: FormGroup;
  public projectInEditMode = false;

  constructor(
    private readonly projectsService: ProjectsService,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    public dialog: MatDialog

  ) { }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUser$.subscribe((user) => {
      this.loggedUser = user;
      this.loggedUserName = `${user.firstName} ${user.lastName}`;
    });
    this.projectId = this.route.snapshot.params.id;

    this.projectsService.getProjectById(this.projectId).subscribe((data: ProjectDTO) => {
        this.project = data;
        if (this.project.remainingDays && (this.project.targetDays - this.project.remainingDays) >= 0) {
          this.withinTarget = 'Yes';
        } else {
          this.withinTarget = 'No';
        }
      });
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public showEditForm(): void {
    this.projectEditForm = this.formBuilder.group({
      name: [this.project.name, [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      description: [this.project.description, [Validators.required, Validators.minLength(10), Validators.maxLength(120)]],
      managementTotalHours: [this.project.managementTotalHours, [Validators.required, Validators.pattern('^[0-9]+$')]],
      managementDailyInput: [this.project.managementDailyHours, [Validators.required, Validators.pattern('^[0-8]')]]
    });
    this.projectInEditMode = !this.projectInEditMode;
  }

  public updateProject(name?: string, description?: string, managementTime?: number): void {
    const editedProject = new UpdateProjectDTO();

    if (name) {
      editedProject.name = name;
    }
    if (description) {
      editedProject.description = description;
    }
    if (managementTime) {
      editedProject.managementTotalHours = managementTime;
    }
    this.projectsService.updateProject(this.projectId, editedProject).subscribe(
      () => {
        this.projectsService.getProjectById(this.projectId).subscribe((updatedProject: ProjectDTO) => {
          this.project = updatedProject;
          if (updatedProject.remainingDays && (updatedProject.targetDays - updatedProject.remainingDays) >= 0) {
            this.withinTarget = 'Yes';
          } else {
            this.withinTarget = 'No';
          }
        });
        this.notificator.success(`Project: ${this.project.name} updated!`);
      },
      (error) => this.notificator.error(JSON.stringify(error.error.error))
    );
  }

  public updateProjectManagerInput(hours: number): void {
    const updatedManagerInput = new UpdateProjectManagerInputDTO();
    updatedManagerInput.managementDailyHours = hours;

    this.projectsService.updateProjectManagerInput(this.projectId, updatedManagerInput).subscribe(
      () => {
        this.projectsService.getProjectById(this.projectId).subscribe((updatedProject: ProjectDTO) => {
          this.project = updatedProject;
          if (updatedProject.remainingDays && (updatedProject.targetDays - updatedProject.remainingDays) >= 0) {
            this.withinTarget = 'Yes';
          } else {
            this.withinTarget = 'No';
          }
        });
        this.authService.getNewToken(+this.loggedUser.id);
        this.notificator.success(`Project: ${this.project.name} updated!`);
      },
      (error) => this.notificator.error(JSON.stringify(error.error.error))
    );
  }

  public stopProject(): void {
    this.projectsService.stopProject(this.projectId).subscribe((project: ProjectDTO) => {
      this.project = project;
      this.authService.getNewToken(+this.loggedUser.id);
      this.notificator.success(`Project: ${project.name} stopped!`);
    });
  }

  public deleteProject(): void {
    this.projectsService.deleteProject(this.projectId).subscribe(() => {
      this.authService.getNewToken(+this.loggedUser.id);
      this.notificator.success(`Project: ${this.project.name} deleted!`);
      this.router.navigate(['projects']);
    });
  }

  agreeDelete() {
    const modalCancel = this.dialog.open(AgreeModalComponent);
    modalCancel.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.deleteProject();
        }
      }
    );
  }

  agreeStop() {
    const modalCancel = this.dialog.open(AgreeModalComponent);
    modalCancel.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.stopProject();
        }
      }
    );
  }
}
