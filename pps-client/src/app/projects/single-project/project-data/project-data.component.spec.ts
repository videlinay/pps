import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDataComponent } from './project-data.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { ProjectsService } from '../../../core/services/projects.service';
import { AuthService } from '../../../core/services/auth.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../../common/userDTOs/user';
import { of } from 'rxjs';
import { ProjectDTO } from '../../models/project.dto';
import { UpdateProjectDTO } from '../../models/update-project.dto';

describe('ProjectDataComponent', () => {
  let projectsService;
  let authService;
  let notificator;
  let router;
  let formBuilder;

  let component: ProjectDataComponent;
  let fixture: ComponentFixture<ProjectDataComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    projectsService = {
      getProjectById() { },
      updateProject() {
        return of();
    },
      updateProjectManagerInput() { },
      stopProject() { },
      deleteProject() { }
    };

    authService = {
      get loggedUser$() {
        return of();
      },
      getNewToken() { }
    };

    notificator = {
      success() { },
      error() { }
    };

    router = {
      navigate() { }
    };

    formBuilder = {
      group() { }
    };

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule, SharedModule],
      declarations: [ProjectDataComponent],
      providers: [ProjectsService, AuthService, NotificatorService]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(ProjectsService, { useValue: projectsService })
      .overrideProvider(NotificatorService, { useValue: notificator })
      .overrideProvider(FormBuilder, { useValue: formBuilder })
      .overrideProvider(ActivatedRoute, { useValue: { snapshot: { params: { id: 1 } } } })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ProjectDataComponent);
        // tslint:disable-next-line: deprecation
        router = TestBed.get(Router);
        component = fixture.componentInstance;
      });
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  describe('ngOnInit() should', () => {
    it('call loggedUser$ once', () => {
      // Arrange
      const mockedUser: User = {
        id: '1',
        email: 'email',
        firstName: 'Pesho',
        lastName: 'Peshov',
        position: 'manager',
        role: 'Basic',
        dateCreated: 'date',
        workingHoursLeft: 8,
        directManagerName: 'self-managed'
      };

      const spy = jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of(mockedUser));
      jest.spyOn(projectsService, 'getProjectById').mockReturnValue(of('project'));

      // Act
      component.ngOnInit();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('subscribe to the loggedUser$ observable and save the emitted value', () => {
      // Arrange
      const mockedUser: User = {
        id: '1',
        email: 'email',
        firstName: 'Pesho',
        lastName: 'Peshov',
        position: 'manager',
        role: 'Basic',
        dateCreated: 'date',
        workingHoursLeft: 8,
        directManagerName: 'self-managed'
      };

      jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of(mockedUser));
      jest.spyOn(projectsService, 'getProjectById').mockReturnValue(of('project'));

      // Act
      component.ngOnInit();

      // Assert
      expect(component.loggedUser).toEqual(mockedUser);
    });

    it('call projectsService.getProjectById() once', () => {
      // Arrange
      const mockedProject: ProjectDTO = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };

      const spy = jest.spyOn(projectsService, 'getProjectById').mockReturnValue(of(mockedProject));
      jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of('user'));

      // Act
      component.ngOnInit();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('call projectsService.getProjectById() with the correct parameter', () => {
      // Arrange
      const mockedProject: ProjectDTO = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };
      const controlParameter = 1;

      const spy = jest.spyOn(projectsService, 'getProjectById').mockReturnValue(of(mockedProject));
      jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of('user'));

      // Act
      component.ngOnInit();

      // Assert
      expect(spy).toBeCalledWith(controlParameter);
    });

    it('subscribe to the projectsService.getProjectById() observable and save the emitted value', () => {
      // Arrange
      const mockedProject: ProjectDTO = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };

      jest.spyOn(projectsService, 'getProjectById').mockReturnValue(of(mockedProject));
      jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of('user'));

      // Act
      component.ngOnInit();

      // Assert
      expect(component.project).toEqual(mockedProject);
    });

    it('subscribe to the projectsService.getProjectById() observable and set the correct value to withinTarget', () => {
      // Arrange
      const mockedProject: ProjectDTO = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };

      const controlValue = 'Yes';

      jest.spyOn(projectsService, 'getProjectById').mockReturnValue(of(mockedProject));
      jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of('user'));

      // Act
      component.ngOnInit();

      // Assert
      expect(component.withinTarget).toEqual(controlValue);
    });
  });

  describe('ngOnDestroy() should', () => {
    it('call unsubscribe() on loggedUserSubscription once', () => {
      // Arrange
      // tslint:disable-next-line: no-string-literal
      component['loggedUserSubscription'] = of(true).subscribe();

      // Act
      component.ngOnDestroy();

      // Assert
      // tslint:disable-next-line: no-string-literal
      expect(component['loggedUserSubscription'].closed).toBeTruthy();
    });
  });

  describe('showEditForm() should', () => {
    it('call formBuilder.group() once', () => {
      // Arrange
      component.project = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };
      const mockedEditForm = 'edit form';

      const spy = jest.spyOn(formBuilder, 'group').mockReturnValue(mockedEditForm);

      // Act
      component.showEditForm();

      // Assert
      expect(spy).toBeCalledTimes(1);
    });

    it('set the postForm filed value', () => {
      // Arrange
      component.project = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };
      const mockedEditForm = 'edit form';

      const spy = jest.spyOn(formBuilder, 'group').mockReturnValue(mockedEditForm);

      // Act
      component.showEditForm();

      // Assert
      expect(component.projectEditForm).toEqual(mockedEditForm);
    });

    it('set the correct value to projectInEditMode', () => {
      // Arrange
      component.project = {
        id: 1,
        name: 'fakeProject',
        description: 'description',
        reporter: 'Pesho Peshov',
        status: 'In Progress',
        targetDays: 10,
        remainingDays: 10,
        managementTotalHours: 1,
        managementHoursPassed: 0,
        managementDailyHours: 1,
      };
      component.projectInEditMode = true;

      const mockedEditForm = 'edit form';

      const spy = jest.spyOn(formBuilder, 'group').mockReturnValue(mockedEditForm);

      // Act
      component.showEditForm();

      // Assert
      expect(component.projectInEditMode).toEqual(false);
    });
  });

  describe('updateProject() should', () => {
    it('call the projectsService.updateProject() once with correct parameters', () => {
      // Arrange
      component.projectId = 1;

      const controlProjectId = 1;

      const controlUpdateData: UpdateProjectDTO = {
        name: 'updatedProject',
        description: 'new description',
        managementTotalHours: 2
      };
      const spy = jest.spyOn(projectsService, 'updateProject').mockReturnValue(of('updated project'));

      // Act
      component.updateProject('updatedProject', 'new description', 2);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(controlProjectId, controlUpdateData);
    });
  });
});
