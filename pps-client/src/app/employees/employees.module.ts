import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListEmployeesComponent } from './list-employees/list-employees.component';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';
import { UsersService } from '../core/services/user.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { EmployeeListItemsComponent } from './employee-list-items/employee-list-items.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeAddSkillComponent } from './employee-add-skill/employee-add-skill.component';

@NgModule({
  declarations: [
    ListEmployeesComponent,
    EmployeeInfoComponent,
    EmployeeListItemsComponent,
    EmployeeComponent,
    EmployeeAddSkillComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    // RouterModule.forChild(routes),
  ],
  providers: [
    UsersService,
  ],
  exports: [
    RouterModule,
  ]
})
export class EmployeesModule { }
