import { EmployeeDTO } from './../models/employee.dto';
import { AddNewSkillDTO } from './../../common/userDTOs/employee-add-skill-dto';
import { EmployeesService } from 'src/app/core/services/employees.service';
import {
  Component,
  OnInit,
} from '@angular/core';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { AdminService } from 'src/app/core/services/admin.service';
import { Skill } from 'src/app/skills/models/skill.dto';
import { SkillsService } from 'src/app/core/services/skills.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee-add-skill',
  templateUrl: './employee-add-skill.component.html',
  styleUrls: ['./employee-add-skill.component.css']
})
export class EmployeeAddSkillComponent implements OnInit {

  public skills: Skill[];
  public skill: AddNewSkillDTO;
  public employeeId: number;
  public employees: EmployeeDTO[];
  public skillControl = new FormControl(this.skill);
  public employeeControl = new FormControl(this.employeeId);

  constructor(
    private readonly notificator: NotificatorService,
    private readonly adminService: AdminService,
    private readonly skillsService: SkillsService,
    private readonly employeeService: EmployeesService,

  ) {}

  ngOnInit(): void {
    this.employeeService.getAllEmployees().subscribe((data: EmployeeDTO[]) => {
      this.employees = data;
    });

    this.skillsService.listAllSkills().subscribe((data: Skill[]) => {
        this.skills = data;
    });
  }

  public addSkill(employee: number, skill: AddNewSkillDTO): void {
    this.adminService.addSkill(employee, skill).subscribe(() => {
      this.notificator.success('Skill added!');
    });
  }



}
