export class EmployeeProjectDTO {
    public projectId: number;
    public projectName: string;
    public skillName: string;
    public dailyTimeInput: number;
}
