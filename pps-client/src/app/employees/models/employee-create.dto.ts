
export class CreateEmployeeDTO {
  public firstName: string;

  public lastName: string;

  public position: string;

  public managerId: number;

  public skillIds: number[];
}
