import { AgreeModalComponent } from './../../shared/components/agree-modal/agree-modal.component';
import { EmployeeDTO } from '../models/employee.dto';
import { AdminService } from './../../core/services/admin.service';
import { EmployeesService } from 'src/app/core/services/employees.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from '../../common/userDTOs/user';

@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.css']
})
export class EmployeeInfoComponent implements OnInit {

  public employeeId: number;
  public employee: EmployeeDTO;
  public loggedUser: User;
  public isAdmin: boolean;
  public skillId: number;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly employeesService: EmployeesService,
    private readonly notificator: NotificatorService,
    private readonly adminService: AdminService,
    private readonly authService: AuthService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.employeeId = +this.route.snapshot.paramMap.get('id');

    this.employeesService.getEmployeeById(this.employeeId).subscribe((data: EmployeeDTO) => {
      this.employee = data;
    });

    this.authService.loggedUser$.subscribe((user: User) => {
      this.loggedUser = user;
      if (user.role === 'Admin') {
        this.isAdmin = true;
      }
    });
  }

  public deleteEmployee(id: number): void {
    this.adminService.deleteEmployee(id).subscribe(() => {
        this.notificator.success('Employee deleted!');
    });
  }

  public navigate(): void {
    this.router.navigate(['/dashboard']);
  }

  agree() {
    const modalCancel = this.dialog.open(AgreeModalComponent);
    modalCancel.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.deleteEmployee(this.employeeId);
          this.navigate();
        }
      }
    );
  }
}
