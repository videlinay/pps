import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';
import {
  FormGroup, FormControl, Validators, FormBuilder
} from '@angular/forms';
import {
  UsersService
} from 'src/app/core/services/user.service';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  User
} from 'src/app/common/userDTOs/user';
import {
  NotificatorService
} from 'src/app/core/services/notificator.service';


@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.css']
})
export class EditUserProfileComponent implements OnInit {

  public editFormGroup: FormGroup;
  public loggedUserId: number;
  public loggedUser: User;

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly notificator: NotificatorService,
    private readonly formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.authService.loggedUser$.subscribe((user: User) => {
      this.loggedUserId = +user.id;
    });

    this.editFormGroup = this.formBuilder.group({
      email: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])]],
      password: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])]],
    });
  }

  public editUser(id: number): void {
    this.usersService.updateUser(id, this.editFormGroup.value).subscribe(() => {
        this.notificator.success('User updated!');
    });
  }
}
