import { EmployeeDTO } from '../../employees/models/employee.dto';
import { ProjectDTO } from '../../projects/models/project.dto';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/core/services/user.service';
import { User } from 'src/app/common/userDTOs/user';


@Component({
  selector: 'app-user-list-items',
  templateUrl: './user-list-items.component.html',
  styleUrls: ['./user-list-items.component.css'],
})
export class UserListItemsComponent implements OnInit {
  public userId: number;
  public user: User;
  public projects: ProjectDTO[];
  public subordinates: EmployeeDTO[];
  public selectedProject: ProjectDTO;
  public selectedEmployee: EmployeeDTO;
  public displayedColumnsProject: string[] = ['id', 'name', 'status', 'targetDays', 'remainingDays'];
  public displayedColumnsEmployee: string[] = ['firstName', 'lastName', 'position'];

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService,
  ) { }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('id');

    this.usersService.getInProgressProjects(this.userId).subscribe((data: ProjectDTO[]) => {
      this.projects = data;
    });

    this.usersService.getUserSubordinates(this.userId).subscribe((data: EmployeeDTO[]) => {
      this.subordinates = data;
    });
  }

  public projectPage(project: ProjectDTO): void {
    this.selectedProject = project;
    this.router.navigate(['/projects', project.id]);
  }

  public employeePage(employee: EmployeeDTO): void {
    this.selectedEmployee = employee;
    this.router.navigate(['/employees', employee.id]);
  }
}


