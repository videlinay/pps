import {
  EmployeeCreateDTO
} from './../../common/userDTOs/employee-create-dto';
import {
  AdminService
} from './../../core/services/admin.service';
import {
  Component,
  OnInit,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import {
  UsersService
} from 'src/app/core/services/user.service';
import {
  User
} from 'src/app/common/userDTOs/user';
import {
  NotificatorService
} from 'src/app/core/services/notificator.service';
import {
  SkillsService
} from 'src/app/core/services/skills.service';
import {
  UserCreateDTO
} from 'src/app/common/userDTOs/user-create-dto';
import { Skill } from 'src/app/skills/models/skill.dto';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  public createEmployeeFormGroup: FormGroup;
  public createManagerFormGroup: FormGroup;
  public skills = new FormControl();
  public loggedUser: User;
  public managerId: number = null;
  public skillsList: Skill[];
  public managersList: User[];
  public isAdmin = false;
  public managerControl = new FormControl(this.managerId);

  constructor(
    private readonly usersService: UsersService,
    private readonly notificator: NotificatorService,
    private readonly skillsService: SkillsService,
    private readonly adminService: AdminService,
    private readonly formBuilder: FormBuilder,

  ) {}

  ngOnInit(): void {
    this.usersService.allUsers().subscribe((data: User[]) => {
      this.managersList = data;
    });

    this.skillsService.listAllSkills().subscribe((data: Skill[]) => {
      this.skillsList = data;
    });


    this.createEmployeeFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
      lastName: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
      position: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
    });

    this.createManagerFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
      lastName: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
      position: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
      email: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
      password: ['', [Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])]],
    });
  }

  public createEmployee(): void {
    const newEmployee: EmployeeCreateDTO = {
      managerId: this.managerId,
      firstName: this.createEmployeeFormGroup.value.firstName,
      lastName: this.createEmployeeFormGroup.value.lastName,
      position: this.createEmployeeFormGroup.value.position,
      skillIds: this.skills.value,
    };

    this.adminService.createEmployee(newEmployee).subscribe(() => {
      this.notificator.success('Employee created!');
    });
  }

  onChangeAdmin() {
    this.isAdmin = !this.isAdmin;
  }

  public createManager(): void {
    if (this.managerId === undefined) {
      this.managerId = null;
    }

    const newManager: UserCreateDTO = {
      directManagerId: this.managerId,
      firstName: this.createManagerFormGroup.value.firstName,
      lastName: this.createManagerFormGroup.value.lastName,
      position: this.createManagerFormGroup.value.position,
      email: this.createManagerFormGroup.value.email,
      password: this.createManagerFormGroup.value.password,
      isAdmin: this.isAdmin
    };

    this.adminService.createManager(newManager).subscribe(() => {
      this.notificator.success('Manager created!');
    });
  }

}
