import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/core/services/user.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public userId: number;

  constructor(
    private readonly route: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('id');
  }
}
