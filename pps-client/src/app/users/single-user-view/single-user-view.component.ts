import { AuthService } from 'src/app/core/services/auth.service';
import {
  Component,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import {
  User
} from 'src/app/common/userDTOs/user';
import {
  UsersService
} from 'src/app/core/services/user.service';
import {
  ActivatedRoute, Router
} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { EditUserProfileComponent } from '../edit-user-modal/edit-user-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { AdminService } from 'src/app/core/services/admin.service';
import { AgreeModalComponent } from 'src/app/shared/components/agree-modal/agree-modal.component';


@Component({
  selector: 'app-single-user-view',
  templateUrl: './single-user-view.component.html',
  styleUrls: ['./single-user-view.component.css']
})
export class SingleUserViewComponent implements OnInit {

  public userId: number;
  public user: User;
  public loggedUser: User;
  public isOwner: boolean;
  public isAdmin: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly adminService: AdminService,
    private readonly notificator: NotificatorService,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.userId = +this.route.snapshot.paramMap.get('id');
    this.usersService.singleUser(this.userId).subscribe((data: User) => {
      this.user = data;

      this.authService.loggedUser$.subscribe((user: User) => {
        this.loggedUser = user;
        if (user.role === 'Admin') {
          this.isAdmin = true;
        }
      });

      if (+this.loggedUser.id === this.userId) {
        this.isOwner = true;
      } else {
        this.isOwner = false;
      }
    });
  }

  openDialog() {
    this.dialog.open(EditUserProfileComponent);
  }

  agree() {
    const modalCancel = this.dialog.open(AgreeModalComponent);
    modalCancel.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.deleteUser(this.userId);
          this.navigate();
        }
      }
    );
  }

  public deleteUser(id: number): void {
    this.adminService.deleteUser(id).subscribe(() => {
        this.notificator.success('User deleted!');
    });
  }
  public navigate(): void {
    this.router.navigate(['/dashboard']);
  }
}
