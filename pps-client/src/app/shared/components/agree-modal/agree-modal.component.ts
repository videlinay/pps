import { Component } from '@angular/core';

@Component({
  selector: 'app-agree-modal',
  templateUrl: './agree-modal.component.html',
  styleUrls: ['./agree-modal.component.css']
})
export class AgreeModalComponent {
  isAgree: boolean;

  constructor(
  ) {
  }

  cancel(): void {
    this.isAgree = false;
  }

  agree(): void {
    this.isAgree = true;
  }
}
