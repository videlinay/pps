import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';

@Injectable({
    providedIn: 'root'
  })
export class RoleGuard implements CanActivate {


  constructor(
      private authService: AuthService,
      private router: Router) {
  }

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = this.authService.loggedUser();

    if (user.role === next.data.role) {
      return true;
    }

    this.router.navigate(['/404']);
    return false;
  }
}
