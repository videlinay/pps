export class UserCreateDTO {
    firstName: string;
    lastName: string;
    position: string;
    directManagerId: number;
    isAdmin: boolean;
    email: string;
    password: string;
  }
