import { Router } from '@angular/router';
import { AuthService } from './core/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from './common/userDTOs/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public loggedIn: boolean;
  public user: User;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {
    // if (authService.isLoggedIn$) {
    //   this.router.navigate(['dashboard']);
    // }
  }

  public ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
     );
  }

  public ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  public logout() {
    this.authService.logout();
  }

}


