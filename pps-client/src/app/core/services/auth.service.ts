import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { User } from 'src/app/common/userDTOs/user';
import { UserLoginDTO } from 'src/app/common/userDTOs/user-login-dto';
import { decode } from 'jwt-decode';
import { UsersService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly serverAddr: string = 'http://localhost:3000';
  private readonly helper = new JwtHelperService();

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserLoggedIn()
  );
  private readonly loggedUserSubject$ = new BehaviorSubject<User>(
    this.loggedUser()
  );

  constructor(
    private readonly usersService: UsersService,
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router,
  ) { }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<User> {
    return this.loggedUserSubject$.asObservable();
  }

  public login(user: UserLoginDTO): Observable<any> {
    return this.http.post<{ token: string }>(
      `${this.serverAddr}/session`, user
    )
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser = this.helper.decodeToken(token);
            this.storage.save('token', token);

            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);
          } catch (error) {
            // error handling on the consumer side
          }
        }),
      );
  }

  public logout() {
    this.storage.save('token', '');
    this.isLoggedInSubject$.next(false);
    this.loggedUserSubject$.next(null);

    this.router.navigate(['home']);
  }

  public getNewToken(userId: number): void {
    this.usersService.getToken(userId).subscribe(({ token }) => {
      this.storage.save('token', token);
      const loggedUser: User = this.helper.decodeToken(token);
      this.isLoggedInSubject$.next(true);
      this.loggedUserSubject$.next(loggedUser);
    });
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

/*   private isUserAdmin(): boolean {

  } */

  public loggedUser(): User {
    try {
      return  this.helper.decodeToken(this.storage.read('token'));
    } catch (error) {
      // in case of storage tampering
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }

  decode() {
    return decode(localStorage.getItem('token'));
  }
}
