import { AddNewSkillDTO } from './../../common/userDTOs/employee-add-skill-dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config/config';
import { UserCreateDTO } from 'src/app/common/userDTOs/user-create-dto';
import { CreateEmployeeDTO } from 'src/app/employees/models/employee-create.dto';


@Injectable({
    providedIn: 'root'
})
export class AdminService {
    constructor(
        private readonly httpClient: HttpClient,
    ) { }



    public createEmployee(body: CreateEmployeeDTO): Observable<CreateEmployeeDTO> {
        return this.httpClient.post<CreateEmployeeDTO>(`${CONFIG.SERVER_ADDR}/admin/employees`, body);
    }

    public createManager(body: UserCreateDTO): Observable<UserCreateDTO> {
        return this.httpClient.post<UserCreateDTO>(`${CONFIG.SERVER_ADDR}/admin/users`, body);
    }

    public deleteEmployee(id: number): Observable<void> {
        return this.httpClient.delete<void>(`${CONFIG.SERVER_ADDR}/admin/employees/${id}`);
    }

    public deleteUser(userId: number): Observable<void> {
        return this.httpClient.delete<void>(`${CONFIG.SERVER_ADDR}/admin/users/${userId}`);
    }

    public addSkill(id: number, body: AddNewSkillDTO): Observable<AddNewSkillDTO> {
        return this.httpClient.post<AddNewSkillDTO>(`${CONFIG.SERVER_ADDR}/admin/employees/${id}`, body);
    }

}

