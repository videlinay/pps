import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Skill } from 'src/app/skills/models/skill.dto';
import { SkillCreateDTO } from 'src/app/skills/models/skill-create.dto';
import { EmployeeDTO } from '../../employees/models/employee.dto';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  constructor(private readonly http: HttpClient) { }

  public listAllSkills(): Observable<Skill[]> {
    return this.http.get<Skill[]>(`${CONFIG.SERVER_ADDR}/skills`);
  }

  public getAllSkillEmployees(id: number): Observable<EmployeeDTO[]> {
    return this.http.get<EmployeeDTO[]>(`${CONFIG.SERVER_ADDR}/skills/${id}/employees`);
  }

  public createSkill(body: SkillCreateDTO): Observable<Skill> {
    return this.http.post<Skill>(`${CONFIG.SERVER_ADDR}/admin/skills`, body);
  }
}
