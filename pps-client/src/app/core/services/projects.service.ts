import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { ProjectDTO } from '../../projects/models/project.dto';
import { CreateProjectDTO } from '../../projects/models/create-project.dto';
import { ProjectEmployeeDTO } from '../../projects/models/project-employee.dto';
import { ProjectSkillDTO } from '../../projects/models/project-skill.dto';
import { UpdateProjectDTO } from '../../projects/models/update-project.dto';
import { UpdateProjectManagerInputDTO } from '../../projects/models/update-manager-input.dto';
import { CreateProjectSkillDTO } from '../../projects/models/create-project-skill.dto';
import { DeleteProjectSkillDTO } from '../../projects/models/delete-project-skill.dto';
import { UpdateProjectSkillDTO } from '../../projects/models/update-project-skill.dto';
import { CreateProjectEmployeeDTO } from '../../projects/models/create-project-employee.dto';
import { UpdateProjectEmployeeDTO } from '../../projects/models/update-project-employee.dto';
import { DeleteProjectEmployeeDTO } from '../../projects/models/delete-project-employee.dto';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {


  constructor(
    private readonly http: HttpClient,
    ) { }


    public getAllProjects(): Observable<ProjectDTO[]> {
        return this.http.get<ProjectDTO[]>(`${CONFIG.SERVER_ADDR}/projects`);
    }

    public getProjectById(projectId: number): Observable<ProjectDTO> {
      return this.http.get<ProjectDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}`);
    }

    public getProjectEmployees(projectId: number): Observable<ProjectEmployeeDTO[]> {
      return this.http.get<ProjectEmployeeDTO[]>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/employees`);
    }

    public createProject(project: CreateProjectDTO): Observable<ProjectDTO> {
      return this.http.post<ProjectDTO>(`${CONFIG.SERVER_ADDR}/projects`, project);
    }

    public updateProject(projectId: number, update: UpdateProjectDTO): Observable<ProjectDTO> {
      return this.http.put<ProjectDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}`, update);
    }

    public updateProjectManagerInput(projectId: number, timeInput: UpdateProjectManagerInputDTO): Observable<ProjectDTO> {
      return this.http.put<ProjectDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/manager`, timeInput);
    }

    public stopProject(projectId: number): Observable<ProjectDTO> {
      return this.http.post<ProjectDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}`, null);
    }

    public deleteProject(projectId: number): Observable<void> {
      return this.http.delete<void>(`${CONFIG.SERVER_ADDR}/projects/${projectId}`);
    }

    public getProjectSkills(projectId: number): Observable<ProjectSkillDTO[]> {
      return this.http.get<ProjectSkillDTO[]>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/skills`);
    }

    public addSkillToProject(projectId: number, skill: CreateProjectSkillDTO): Observable<ProjectSkillDTO> {
      return this.http.post<ProjectSkillDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/skills`, skill);
    }

    public updateProjectSkillTime(projectId: number, update: UpdateProjectSkillDTO): Observable<ProjectSkillDTO> {
      return this.http.put<ProjectSkillDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/skills`, update);
    }

    public removeSkillFromProject(projectId: number, skill: DeleteProjectSkillDTO): Observable<void> {
      return this.http.request<void>('delete', `${CONFIG.SERVER_ADDR}/projects/${projectId}/skills`, { body: skill});
    }

    public addEmployeeToProject(projectId: number, employee: CreateProjectEmployeeDTO): Observable<ProjectEmployeeDTO> {
      return this.http.post<ProjectEmployeeDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/employees`, employee);
    }

    public updateProjectEmployeeTime(projectId: number, update: UpdateProjectEmployeeDTO): Observable<ProjectEmployeeDTO> {
      return this.http.put<ProjectEmployeeDTO>(`${CONFIG.SERVER_ADDR}/projects/${projectId}/employees`, update);
    }

    public removeEmployeeFromProject(projectId: number, employee: DeleteProjectEmployeeDTO): Observable<void> {
      return this.http.request<void>('delete', `${CONFIG.SERVER_ADDR}/projects/${projectId}/employees`, { body: employee});
    }
}
