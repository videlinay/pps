import { UsersComponent } from './users/users/users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './common/guards/auth.guard';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoleGuard } from './common/guards/role.guard';
import { ListSkillsComponent } from './skills/list-skills/list-skills.component';
import { AddSkillComponent } from './skills/add-skill/add-skill.component';
import { ListProjectsComponent } from './projects/list-projects/list-projects.component';
import { CreateProjectComponent } from './projects/create-project/create-project.component';
import { SingleProjectComponent } from './projects/single-project/single-project.component';
import { ServerErrorComponent } from './server-error/server-error.component';
import { ListEmployeesComponent } from './employees/list-employees/list-employees.component';
import { EmployeeComponent } from './employees/employee/employee.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'projects',
    component: ListProjectsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'projects/create',
    component: CreateProjectComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'projects/:id',
    component: SingleProjectComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees',
    component: ListEmployeesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees/:id', component: EmployeeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users/:id', component: UsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'skills',
    component: ListSkillsComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {role: 'Admin'}
  },
  {
    path: 'skills/create',
    component: AddSkillComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {role: 'Admin'}
  },
/*  lazy loading
 {
    path: 'projects',
    loadChildren: () => import('./projects/projects.module')
    .then(m => m.ProjectsModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'employees',
    loadChildren: () => import('./employees/employees.module')
    .then( m => m.EmployeesModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'skills',
    loadChildren: () => import('./skills/skills.module').then(
      m => m.SkillsModule
    ), canActivate: [
      AuthGuard,
      RoleGuard
    ],
  }
  */
  { path: 'not-found', component: PageNotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes/* , {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
    } */),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
