import { EditUserProfileComponent } from '../../users/edit-user-modal/edit-user-modal.component';
import {
  Component,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  User
} from 'src/app/common/userDTOs/user';
import {
  UsersService
} from 'src/app/core/services/user.service';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-dashboard-user-info',
  templateUrl: './dashboard-user-info.component.html',
  styleUrls: ['./dashboard-user-info.component.css']
})
export class DashboardUserInfoComponent implements OnInit {
  public loggedUserId: number;
  public loggedUser: User;
  public editFormGroup: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.authService.loggedUser$.subscribe((user) => {
      this.loggedUserId = +user.id;
    });

    this.usersService.singleUser(this.loggedUserId).subscribe((data) => {
      this.loggedUser = data;
    });
  }
  openDialog() {
    this.dialog.open(EditUserProfileComponent);
  }
}
