import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SkillsService } from 'src/app/core/services/skills.service';
import { ActivatedRoute } from '@angular/router';
import { SkillCreateDTO } from '../models/skill-create.dto';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.css']
})
export class AddSkillComponent implements OnInit {

  public skillName: string;
  public skillCreateBody: SkillCreateDTO;
  constructor(
    private readonly service: SkillsService,
    private readonly notificator: NotificatorService,
  ) { }

  @Output()
  skillAdded: EventEmitter<void> = new EventEmitter<void>();

  ngOnInit(): void {
  }

  public createSkill(): void {
    this.skillCreateBody = {
      name: this.skillName
    };
    if (this.skillName == null) {
      this.notificator.error('Please, enter a Skill');
    } else {
      this.service.createSkill(this.skillCreateBody)
        .subscribe({
          next: data => this.skillAdded.emit(),
          error: e => this.notificator.error('This skill already exists!'),

        });
    }
  }

}

