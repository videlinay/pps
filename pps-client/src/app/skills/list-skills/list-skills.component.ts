import { Component, OnInit } from '@angular/core';
import { SkillsService } from 'src/app/core/services/skills.service';
import { Skill } from '../models/skill.dto';

@Component({
  selector: 'app-list-skills',
  templateUrl: './list-skills.component.html',
  styleUrls: ['./list-skills.component.css']
})
export class ListSkillsComponent implements OnInit {
  public skills: Skill[];
  public show: boolean;

  constructor(
    private service: SkillsService,

  ) { }

  ngOnInit(): void {
    this.service.listAllSkills()
    .subscribe(
      (data: Skill[]) => this.skills = data
      );
  }
  public toggleAddSkillForm() {
    this.show = !this.show;
  }

  public refreshSkillsList($event) {
    this.ngOnInit();
  }

}
