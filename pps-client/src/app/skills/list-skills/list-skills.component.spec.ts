import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSkillsComponent } from './list-skills.component';
import { SkillsService } from 'src/app/core/services/skills.service';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Skill } from '../models/skill.dto';

describe('ListSkillsComponent', () => {
  let component: ListSkillsComponent;
  let fixture: ComponentFixture<ListSkillsComponent>;
  let skills: Skill[];
  const skillsService = {
    listAllSkills(){},
    getAllSkillEmployees(){},
    createSkill(){},
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSkillsComponent ],
      imports: [MaterialModule],
    })
    .overrideProvider(SkillsService, { useValue: skillsService })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

/*   it('should update logged on init', (done) => {
    isLogged$.subscribe(state => {
      expect(component.logged).toBe(true);
      done();
    });
  }); */

});
