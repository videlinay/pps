
<img src="./screenshots/logo.png" alt="logo" width="100px" style="margin: -20px auto;"/> 
##<span style="color:#453d93">SMART</span><span style="color:#567cad"> PROJECT</span><span style="color:#453d93"> PLANNER</span>
#### Team 19
#### Angular & NestJS Final Project

<hr>

## How to setup

1. Go to pps-server folder and open the terminal, then run:

   - `npm install`

<br>

2. Setup MySQL Database with new Schema with the name pps (should be the same as in .env and ormconfig.json).

<br>

3. Setup `.env` and `оrmconfig.json` files. They need to be on root level in pps-server folder where is `package.json` and other config files.

<br>

   - `.env` file with your settings:

   ```javascript
   PORT = 3000
   DB_TYPE = mysql
   DB_HOST = localhost
   DB_PORT = 3306
   DB_USERNAME = root
   DB_PASSWORD = root
   DB_DATABASE_NAME = pps
   JWT_SECRET = secretkey
   ```

   - `ormconfig.json` file with your settings:

   ```javascript
    {
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "pps",
    "entities": [
        "src/database/entities/**/*.ts"
    ],
    "migrations": [
        "src/database/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/database/entities",
        "migrationsDir": "src/database/migration"
    }
   }
   ```

   <br>

4. After files are setup go in terminal and run:

   - `npm run start:dev` or `npm run start`

**Now you will have working server with empty database. If you want to load information in database you should run `npm run seed` command in the terminal.**

5. Go to pps-client folder and open the terminal, then run:

   - `npm install`

<br>


6. Now you can start angular server from terminal with:

   - `ng serve`

<br>

You should see a message on the console **"Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/. Compiled successfully"**.

## Login

1. Admin
  email - `admin@admin.com`
  password - `Admin`

<br>

2. Basic User
  email - `manager@manager.com`
  password - `Manager`

## Tests

1. You can run tests from client folder with:

   - `npm run test`

## Schreenshots

#### Home and Login

<img src="./screenshots/homepage_login.png" alt="home"/> 

#### Dashboard

<img src="./screenshots/dashboard.png" alt="dashboard"/> 

#### Projects List

<img src="./screenshots/projects_list.png" alt="projects"/> 

#### Dedicated Project Page

<img src="./screenshots/single_project.png" alt="project"/> 

#### Create Project

<img src="./screenshots/create_project.png" alt="create_project"/> 

#### Edit Project

<img src="./screenshots/edit_project.png" alt="edit_project"/> 

#### Employees List

<img src="./screenshots/employees.png" alt="employees"/> 

#### Create Employee or User

<img src="./screenshots/create_employee.png" alt="create_employee"/> 

#### Skills

<img src="./screenshots/skills.png" alt="skills"/> 
