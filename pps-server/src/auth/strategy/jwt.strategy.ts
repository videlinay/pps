import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ReturnUserDTO } from '../../users/models/user-return.dto';
import { UsersService } from '../../users/users.service';
import { ProjectPlanningSystemError } from 'src/common/exceptions/pps.error';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersService: UsersService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: ReturnUserDTO): Promise<ReturnUserDTO> {
    const user: ReturnUserDTO = await this.usersService.getSingleUser(payload.id);

    if (!user) {
      throw new ProjectPlanningSystemError('Please login to continue!', 401);
    }

    return user;
  }
}
