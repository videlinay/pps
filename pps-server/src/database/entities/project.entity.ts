/* eslint-disable @typescript-eslint/no-unused-vars */
// tslint:disable-next-line:max-line-length
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { ProjectStatus } from '../../projects/enums/project-status.enum';
import { ProjectSkill } from './projectSkill.entity';
import { ProjectEmployee } from './projectEmployee.entity';

@Entity('projects')
export class Project {
  @PrimaryGeneratedColumn('increment')
    public id: number;

  @Column('nvarchar')
    public name: string;

  @Column('nvarchar')
    public description: string;

  @Column('nvarchar')
    public reporter: string;

  @Column({
    type: 'enum',
    enum: ProjectStatus,
    default: ProjectStatus.ONGOING,
  })
    public status: ProjectStatus;

  @Column('int')
    public targetDays: number;

  @Column({ type: 'int', default: 0, nullable: true })
    public remainingDays: number;

  @Column('int')
    public managementTotalHours: number;

  @Column({ type: 'int', default: 0 })
    public managementHoursPassed: number;

  @Column('int')
    public managementDailyHours: number;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @Column({ type: 'boolean', default: true })
  public isNew: boolean;

  @ManyToOne(type => User, user => user.projects)
    public manager: Promise<User>;

  @OneToMany(type => ProjectSkill, projectSkill => projectSkill.project)
    public projectSkills: Promise<ProjectSkill[]>;

  @OneToMany(type => ProjectEmployee, projectEmployee => projectEmployee.project)
    public projectEmployees: Promise<ProjectEmployee[]>;
}
