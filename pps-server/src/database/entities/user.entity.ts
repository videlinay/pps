/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { Project } from './project.entity';
import { Employee } from './employee.entity';
import { UserRole } from './../../users/enum/user-roles.enum';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('nvarchar', { nullable: false, length: 10 })
  public firstName: string;

  @Column('nvarchar', { nullable: false, length: 10 })
  public lastName: string;

  @Column('nvarchar', { nullable: false, unique: true })
  public email: string;

  @Column('nvarchar', { nullable: false })
  public password: string;

  @Column('nvarchar', { default: 'Manager' })
  public position: string;

  @Column('nvarchar')
  public directManagerName: string;

  @Column({ type: 'int', default: 8 })
  public workingHoursLeft: number;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @Column('enum', { enum: UserRole, nullable: false, default: UserRole.Basic })
  public role: UserRole;

  @ManyToOne(type => User, user => user.dependentManagers)
  public directManager: Promise<User>;

  @OneToMany(type => User, user => user.directManager)
  public dependentManagers: Promise<User[]>;

  @OneToMany(type => Project, project => project.manager)
  public projects: Promise<Project[]>;

  @OneToMany(type => Employee, employee => employee.manager)
  public subordinates: Promise<Employee[]>;
}
