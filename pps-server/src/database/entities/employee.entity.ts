/* eslint-disable @typescript-eslint/no-unused-vars */
import { PrimaryGeneratedColumn, Entity, Column, ManyToOne, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { Project } from './project.entity';
import { Skill } from './skill.entity';
import { ProjectEmployee } from './projectEmployee.entity';

@Entity('employees')
export class Employee {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('nvarchar')
  public firstName: string;

  @Column('nvarchar')
  public lastName: string;

  @Column('nvarchar')
  public position: string;

  @Column('nvarchar')
  public directManager: string;

  @Column({ type: 'int', default: 8 })
  public workingHoursLeft: number;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @ManyToOne(type => User, user => user.subordinates)
  public manager: Promise<User>;

  @ManyToMany(type => Skill, skill => skill.employees)
  @JoinTable()
  public skills: Promise<Skill[]>;

  @OneToMany(type => ProjectEmployee, projectEmployee => projectEmployee.employee)
  public projectEmployees: Promise<ProjectEmployee[]>;
}
