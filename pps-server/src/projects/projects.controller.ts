import { Controller, Get, UseGuards, Param, ParseIntPipe, Body, Post, Delete, Put, Query } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectReturnDTO } from './models/project-return.dto';
import { ProjectUpdateDTO } from './models/project-update.dto';
import { AddSkillToProjectDTO } from './models/add-skill.dto';
import { UpdateProjectSkillTimeDTO } from './models/update-skill.dto';
import { RemoveProjectSkillDTO } from './models/remove-skill.dto';
import { ReturnProjectSkillDTO } from './models/return-project-skill.dto';
import { ProjectSkillsService } from './project-skills.service';
import { ReturnProjectEmployeeDTO } from './models/return-project-employee.dto';
import { ProjectEmployeesService } from './project-employees.service';
import { AddEmployeeToProjectDTO } from './models/add-employee.dto';
import { UpdateProjectEmployeeTimeDTO } from './models/update-employee.dto';
import { RemoveProjectEmployeeDTO } from './models/remove-employee.dto';
import { ConstructProjectDTO } from './models/construct-project.dto';
import { QueryProjectDTO } from './models/query-project.dto';
import { ReturnUserDTO } from '../users/models/user-return.dto';
import { UpdateProjectManagerTimeDTO } from './models/update-manager-time.dto';
import { User } from '../common/decorators/user.decorator';
import { AuthenticationGuard } from '../common/guards/auth.guard';

@Controller('projects')
@UseGuards(AuthenticationGuard)
export class ProjectsController {

  public constructor(
        private readonly projectsService: ProjectsService,
        private readonly projectSkillsService: ProjectSkillsService,
        private readonly projectEmployeesService: ProjectEmployeesService,
    ) {}

  @Get()
    public async getAllProjects(@Query() query: QueryProjectDTO): Promise<ProjectReturnDTO[]> {
    return await this.projectsService.getAllProjects(query);
  }

  @Get(':projectId')
  public async getProjectById(
      @Param('projectId', ParseIntPipe) id: number): Promise<ProjectReturnDTO> {
    return await this.projectsService.getProjectById(id);
  }

  @Post()
public async createProject(
  @Body() body: ConstructProjectDTO, @User() user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    return await this.projectsService.createProject(user, body);
  }

  @Put(':projectId')
  public async updateProject(
      @Param('projectId', ParseIntPipe) id: number,
      @Body() body: ProjectUpdateDTO,
      @User() user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    return await this.projectsService.updateProject(id, body, user);
  }

  @Put(':projectId/manager')
  public async updateProjectManagerTime(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: UpdateProjectManagerTimeDTO,
    @User() user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    return await this.projectsService.updateProjectManagerTime(id, body, user);
  }

  @Delete(':projectId')
  public async deleteProject(
    @Param('projectId', ParseIntPipe) id: number,
    @User() user: ReturnUserDTO): Promise<void> {
    return await this.projectsService.deleteProject(id, user);
  }

  @Post(':projectId')
  public async stopProject(
    @Param('projectId', ParseIntPipe) id: number,
    @User() user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    return await this.projectsService.stopProject(id, user);
  }

  @Get(':projectId/skills')
  public async getAllProjectSkills(
    @Param('projectId', ParseIntPipe) id: number): Promise<ReturnProjectSkillDTO[]> {
    return await this.projectSkillsService.getAllProjectSkills(id);
  }

  @Post(':projectId/skills')
  public async addSkillToProject(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: AddSkillToProjectDTO,
    @User() user: ReturnUserDTO): Promise<ReturnProjectSkillDTO> {
    return await this.projectSkillsService.addSkillToProject(id, body, user);
  }

  @Put(':projectId/skills')
  public async updateProjectSkillTime(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: UpdateProjectSkillTimeDTO,
    @User() user: ReturnUserDTO): Promise<ReturnProjectSkillDTO> {
    return await this.projectSkillsService.updateProjectSkillTime(id, body, user);
  }

  @Delete(':projectId/skills')
  public async removeSkillFromProject(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: RemoveProjectSkillDTO,
    @User() user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    return await this.projectSkillsService.removeSkillFromProject(id, body, user);
  }

  @Get(':projectId/employees')
  public async getAllProjectEmployees(
    @Param('projectId', ParseIntPipe) id: number): Promise<ReturnProjectEmployeeDTO[]> {
    return await this.projectEmployeesService.getAllProjectEmployees(id);
  }

  @Post(':projectId/employees')
  public async addEmployeeToProject(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: AddEmployeeToProjectDTO,
    @User() user: ReturnUserDTO): Promise<ReturnProjectEmployeeDTO> {
    return await this.projectEmployeesService.addEmployeeToProject(id, body, user);
  }

  @Put(':projectId/employees')
  public async updateProjectEmployeeTime(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: UpdateProjectEmployeeTimeDTO,
    @User() user: ReturnUserDTO): Promise<ReturnProjectEmployeeDTO> {
    return await this.projectEmployeesService.updateProjectEmployeeTime(id, body, user);
  }

  @Delete(':projectId/employees')
  public async removeEmployeeFromProject(
    @Param('projectId', ParseIntPipe) id: number,
    @Body() body: RemoveProjectEmployeeDTO,
    @User() user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    return await this.projectEmployeesService.removeEmployeeFromProject(id, body, user);
  }
}
