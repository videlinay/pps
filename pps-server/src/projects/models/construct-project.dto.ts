import { ProjectCreateDTO } from './project-create.dto';
import { AddSkillToProjectDTO } from './add-skill.dto';
import { AddEmployeeToProjectDTO } from './add-employee.dto';
import { ArrayUnique, ArrayNotEmpty, IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class ConstructProjectDTO {
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => ProjectCreateDTO)
  public data: ProjectCreateDTO;

  @ArrayNotEmpty()
  @ArrayUnique()
  @ValidateNested()
  @Type(() => AddSkillToProjectDTO)
  public skills: AddSkillToProjectDTO[];

  @ArrayUnique()
  @ValidateNested()
  @Type(() => AddEmployeeToProjectDTO)
  public employees: AddEmployeeToProjectDTO[];
}
