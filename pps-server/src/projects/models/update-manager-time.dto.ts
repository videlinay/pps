import {  Min } from 'class-validator';

export class UpdateProjectManagerTimeDTO {
  @Min(0)
    public managementDailyHours: number;
}
