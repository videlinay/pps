import { Min, IsPositive } from 'class-validator';

export class AddEmployeeToProjectDTO {
  @Min(1)
    public skillId: number;

  @Min(1)
    public employeeId: number;

  @IsPositive()
    public dailyTimeInput: number;
}
