import { Expose } from 'class-transformer';
import { ProjectStatus } from '../enums/project-status.enum';

export class ProjectReturnDTO {
  @Expose()
  public id: number;

  @Expose()
  public name: string;

  @Expose()
  public description: string;

  @Expose()
  public reporter: string;

  @Expose()
  public status: ProjectStatus;

  @Expose()
  public targetDays: number;

  @Expose()
  public remainingDays: number;

  @Expose()
  public managementTotalHours: number;

  @Expose()
  public managementHoursPassed: number;

  @Expose()
  public managementDailyHours: number;
}
