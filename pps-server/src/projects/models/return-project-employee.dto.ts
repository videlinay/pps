import { Expose } from 'class-transformer';

export class ReturnProjectEmployeeDTO {
  @Expose()
    public id: number;

  @Expose()
    public employeeName: string;

  @Expose()
    public skillName: string;

  @Expose()
    public dailyTimeInput: number;
}
