import { Min } from 'class-validator';

export class RemoveProjectSkillDTO {
  @Min(1)
    public projectSkillId: number;
}
