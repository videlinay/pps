import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Project } from '../database/entities/project.entity';
import { ProjectReturnDTO } from './models/project-return.dto';
import { plainToClass } from 'class-transformer';
import { ProjectStatus } from './enums/project-status.enum';
import { ProjectUpdateDTO } from './models/project-update.dto';
import { ProjectCreateDTO } from './models/project-create.dto';
import { User } from '../database/entities/user.entity';
import { ReturnUserDTO } from '../users/models/user-return.dto';
import { ProjectPlanningSystemError } from '../common/exceptions/pps.error';
import { ConstructProjectDTO } from './models/construct-project.dto';
import { ProjectSkillsService } from './project-skills.service';
import { ProjectEmployeesService } from './project-employees.service';
import { AddEmployeeToProjectDTO } from './models/add-employee.dto';
import { QueryProjectDTO } from './models/query-project.dto';
import { UpdateProjectManagerTimeDTO } from './models/update-manager-time.dto';
import { ReturnProjectSkillDTO } from './models/return-project-skill.dto';
import { ReturnProjectEmployeeDTO } from './models/return-project-employee.dto';
import { ProjectSkill } from '../database/entities/projectSkill.entity';
import { ProjectEmployee } from '../database/entities/projectEmployee.entity';
import { Cron } from '@nestjs/schedule';
import { Employee } from '../database/entities/employee.entity';
import { AddSkillToProjectDTO } from './models/add-skill.dto';

@Injectable()
export class ProjectsService {

  public constructor(
        @InjectRepository(ProjectEmployee)
         private readonly projectEmployeeRepo: Repository<ProjectEmployee>,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(Project) private readonly projectRepository: Repository<Project>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(ProjectSkill) private readonly projectSkillRepo: Repository<ProjectSkill>,
        @Inject(forwardRef(() => ProjectSkillsService))
        private readonly projectSkillsService: ProjectSkillsService,
        @Inject(forwardRef(() => ProjectEmployeesService))
        private readonly projectEmployeesService: ProjectEmployeesService,
        ) {}

  public async getAllProjects(query: QueryProjectDTO): Promise<ProjectReturnDTO[]> {
    let projects: Project[] = await this.projectRepository.find(
      { where: { isDeleted: false }, order: { id: 'DESC' } });

    if (query.status === 'In Progress') {
      projects = projects.filter(project => project.status === ProjectStatus.ONGOING);
    }
    if (query.status === 'Ended') {
      projects = projects.filter(project => project.status === ProjectStatus.CLOSED);
    }
    if (query.name) {
      projects = projects.filter(project =>
        project.name.toLowerCase().includes(query.name.toLowerCase()));
    }
    if (query.description) {
      projects = projects.filter(project =>
        project.description.toLowerCase().includes(query.description.toLowerCase()));
    }

    return plainToClass(ProjectReturnDTO, projects, { excludeExtraneousValues: true });
  }

  public async getProjectById(projectId: number): Promise<ProjectReturnDTO> {
    const project: Project = await this.findProjectById(projectId);

    return plainToClass(ProjectReturnDTO, project, { excludeExtraneousValues: true });
  }

  public async createProject(
    user: ReturnUserDTO, project: ConstructProjectDTO): Promise<ProjectReturnDTO> {
    this.validateProjectAssignments(project.skills, project.employees);

    const newProject: Project = await this.initializeProject(project.data, user);

    setTimeout(async () => await this.projectRepository.update(
      // tslint:disable-next-line: align
      newProject.id, { isNew: false }), 4000);

    await Promise.all(project.skills.map(async skill =>
       await this.projectSkillsService.addSkillToProject(newProject.id, skill, user)));

    await this.calculateProjectRemainingDays(newProject.id);

    await Promise.all(project.employees.map(async employee =>
      await this.projectEmployeesService.validateEmployeeProjectAssignment(newProject, employee)));

    await Promise.all(project.employees.map(async employee =>
       await this.projectEmployeesService.addEmployeeToProject(newProject.id, employee, user)));

    this.deductEmployeesWorkingHours(project.employees);
    this.calculateProjectRemainingDays(newProject.id);

    return plainToClass(ProjectReturnDTO, newProject, { excludeExtraneousValues: true });
  }

  private async initializeProject(
    project: ProjectCreateDTO, user: ReturnUserDTO): Promise<Project> {
    const manager: User = await this.userRepository.findOne(user.id);

    if (project.managementDailyHours === 0 && project.managementTotalHours > 0) {
      throw new ProjectPlanningSystemError('You need to input daily hours for management!', 400);
    }
    if (project.managementDailyHours > 0 && project.managementTotalHours === 0) {
      throw new ProjectPlanningSystemError('You need to input total time for management!', 400);
    }
    if (manager.workingHoursLeft < project.managementDailyHours) {
      throw new ProjectPlanningSystemError('You do not have sufficient free working hours to start this project!', 400);
    }
    const newProject: Project = this.projectRepository.create(project);
    newProject.reporter = `${manager.firstName} ${manager.lastName}`;
    newProject.manager = Promise.resolve(manager);
    newProject.projectSkills = Promise.resolve([]);
    newProject.projectEmployees = Promise.resolve([]);

    await this.projectRepository.save(newProject);

    const adjustedWorkingHours: number = manager.workingHoursLeft - project.managementDailyHours;

    await this.userRepository.update(manager.id, { workingHoursLeft: adjustedWorkingHours });

    this.calculateProjectRemainingDays(newProject.id);

    return newProject;
  }

  public async updateProject(
    projectId: number, update: ProjectUpdateDTO, user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    let project: Project = await this.findProjectById(projectId);

    if (project.status === ProjectStatus.CLOSED) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
    }
    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to edit project with ID: ${project.id}!`, 401);
    }
    if (update.managementTotalHours < project.managementHoursPassed) {
      throw new ProjectPlanningSystemError('New management total hours already passed!', 400);
    }
    project = { ...project, ...update };

    await this.projectRepository.save(project);

    if (update.managementTotalHours) {
      this.calculateProjectRemainingDays(project.id);
    }

    return plainToClass(ProjectReturnDTO, project, { excludeExtraneousValues: true });
  }

  public async updateProjectManagerTime(
    projectId: number, update: UpdateProjectManagerTimeDTO,
    user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    const project: Project = await this.findProjectById(projectId);

    if (project.status === ProjectStatus.CLOSED) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
    }
    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to edit project with ID: ${project.id}!`, 401);
    }
    if ((update.managementDailyHours > 0 && project.managementTotalHours === 0) ||
    (update.managementDailyHours > 0 && project.managementHoursPassed >=
    project.managementTotalHours)) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} does not currently require management!`, 400);
    }
    const manager: User = await project.manager;
    manager.workingHoursLeft += project.managementDailyHours;

    if (manager.workingHoursLeft < update.managementDailyHours) {
      throw new ProjectPlanningSystemError('You do not have sufficient free working hours!', 400);
    }
    manager.workingHoursLeft -= update.managementDailyHours;

    await this.userRepository.save(manager);

    await this.projectRepository.update(project.id, {
      managementDailyHours: update.managementDailyHours });

    this.calculateProjectRemainingDays(project.id);

    return plainToClass(ProjectReturnDTO, project, { excludeExtraneousValues: true });
  }

  public async deleteProject(projectId: number, user: ReturnUserDTO): Promise<void> {
    const project: Project = await this.findProjectById(projectId);

    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to delete project with ID: ${project.id}!`, 401);
    }
    const projectEmployees: ProjectEmployee[] = (await project.projectEmployees).filter(
      projectEmployee => !projectEmployee.isDeleted);

    if (projectEmployees.length) {
      await this.restoreEmployeesWorkingHours(projectEmployees);
    }
    const manager: User = await project.manager;

    const adjustedWorkingHours: number = manager.workingHoursLeft + project.managementDailyHours;

    await this.userRepository.update(manager.id, { workingHoursLeft: adjustedWorkingHours });
    await this.projectRepository.update(project.id, { isDeleted: true });
  }

  public async stopProject(projectId: number, user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    const project: Project = await this.findProjectById(projectId);

    if (project.status === ProjectStatus.CLOSED) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} already stopped!`, 400);
    }
    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to stop project with ID: ${project.id}!`, 401);
    }
    const projectEmployees: ProjectEmployee[] = (await project.projectEmployees).filter(
      projectEmployee => !projectEmployee.isDeleted);

    if (projectEmployees.length) {
      await this.restoreEmployeesWorkingHours(projectEmployees);
    }
    const manager: User = await project.manager;
    const adjustedWorkingHours: number = manager.workingHoursLeft + project.managementDailyHours;

    await this.userRepository.update(manager.id, { workingHoursLeft: adjustedWorkingHours });

    project.managementDailyHours = 0;
    project.status = ProjectStatus.CLOSED;

    await this.projectRepository.save(project);

    return plainToClass(ProjectReturnDTO, project, { excludeExtraneousValues: true });
  }

  public async findProjectById(projectId: number): Promise<Project> {
    const project: Project = await this.projectRepository.findOne(
      projectId, { where: { isDeleted: false } });

    if (project === undefined) {
      throw new ProjectPlanningSystemError(`Project with ID: ${projectId} does not exist!`, 404);
    }

    return project;
  }

  public async calculateProjectRemainingDays(projectId: number): Promise<void> {
    const projectSkills: ReturnProjectSkillDTO[] =
    await this.projectSkillsService.getAllProjectSkills(projectId);

    const projectEmployees: ReturnProjectEmployeeDTO[] =
    await this.projectEmployeesService.getAllProjectEmployees(projectId);

    const project: Project = await this.findProjectById(projectId);

    const activeProjectSkills: ReturnProjectSkillDTO[] = projectSkills.filter(
      projectSkill => projectSkill.totalHours > projectSkill.hoursWorked &&
      projectSkill.totalHours > 0);

    const remainingDaysForSkills: number[] = activeProjectSkills.map((projectSkill) => {
      const employeesForEachSkill: ReturnProjectEmployeeDTO[] = projectEmployees.filter(
        employee => employee.skillName === projectSkill.skillName);
      if (!employeesForEachSkill.length) {
        return null;
      }
      const dailyHoursForEachSkill: number = employeesForEachSkill.reduce(
          (acc, employee) => { return acc + employee.dailyTimeInput; }, 0);

      return Math.ceil((
        projectSkill.totalHours - projectSkill.hoursWorked) / dailyHoursForEachSkill);
    });
    if (!remainingDaysForSkills.length) {
      remainingDaysForSkills.push(0);
    }
    if (project.managementTotalHours > 0 && project.managementDailyHours > 0) {
      const remainingDaysForManagement: number = Math.ceil((project.managementTotalHours
        - project.managementHoursPassed) / project.managementDailyHours);
      remainingDaysForSkills.push(remainingDaysForManagement);
    } else if (project.managementTotalHours > 0 && project.managementDailyHours === 0 &&
      project.managementHoursPassed < project.managementTotalHours) {
      remainingDaysForSkills.push(null);
    }
    remainingDaysForSkills.sort((a, b) => b - a);

    project.remainingDays = remainingDaysForSkills.includes(null)
     ? null : remainingDaysForSkills[0];

    await this.projectRepository.save(project);
  }

  @Cron('0 30 18 * * 1-5')
  private async updateProjectsHoursWorked(): Promise<void> {
    const allActiveProjects: Project[] = await this.projectRepository.find(
      { where: { status: ProjectStatus.ONGOING , isDeleted: false } });

    allActiveProjects.forEach(async (project) => {
      let unfinishedProjectSkills: ProjectSkill[] = (await project.projectSkills).filter(
        projectSkill => !projectSkill.isDeleted && projectSkill.totalHours > 0 &&
        projectSkill.hoursWorked < projectSkill.totalHours);

      const projectEmployees: ProjectEmployee[] = (await project.projectEmployees).filter(
        projectEmployee => !projectEmployee.isDeleted);

      const projectEmployeesToDelete: ProjectEmployee[] = [];

      unfinishedProjectSkills.forEach(async (projectSkill) => {
        const projectSkillEmployees: ProjectEmployee[] = [];

        const dailyHoursWorked: number = projectEmployees.reduce((acc, projectEmployee) => {
          if (projectEmployee.skillName === projectSkill.skillName) {
            projectSkillEmployees.push(projectEmployee);

            return acc + projectEmployee.dailyTimeInput;
          }
          return acc;
        // tslint:disable-next-line: align
        }, 0);
        projectSkill.hoursWorked += dailyHoursWorked;

        if (projectSkill.hoursWorked >= projectSkill.totalHours) {
          projectSkill.hoursWorked = projectSkill.totalHours;

          projectSkillEmployees.forEach(projectSkillEmployee =>
            projectEmployeesToDelete.push(projectSkillEmployee));
        }
        await this.projectSkillRepo.save(projectSkill);
      });
      this.restoreEmployeesWorkingHours(projectEmployeesToDelete);

      if (project.managementHoursPassed < project.managementTotalHours) {
        project.managementHoursPassed += project.managementDailyHours;
      }
      if (project.managementHoursPassed >= project.managementTotalHours) {
        project.managementHoursPassed = project.managementTotalHours;

        const manager: User = await project.manager;

        const adjustedWorkingHours: number = manager.workingHoursLeft
        + project.managementDailyHours;

        await this.userRepository.update(manager.id, { workingHoursLeft: adjustedWorkingHours });

        project.managementDailyHours = 0;
      }
      if (project.targetDays > 0) {
        project.targetDays -= 1;
      }
      await this.projectRepository.save(project);

      unfinishedProjectSkills = (await project.projectSkills).filter(
        projectSkill => !projectSkill.isDeleted && projectSkill.totalHours > 0 &&
        projectSkill.hoursWorked < projectSkill.totalHours);

      if (project.managementHoursPassed === project.managementTotalHours &&
        !unfinishedProjectSkills.length) {
        await this.stopProject(project.id, await project.manager);
      }
      this.calculateProjectRemainingDays(project.id);
    });
  }

  private async deductEmployeesWorkingHours(assignments: AddEmployeeToProjectDTO[]): Promise<void> {
    const employeeIds: number[] = assignments.map(assignment => assignment.employeeId);
    const uniqueIds: number[] = employeeIds.filter((id, i) => employeeIds.indexOf(id) === i);

    const updatedAssignments: AddEmployeeToProjectDTO[] = uniqueIds.map((uniqueId) => {
      const employeeAssignmentTime: number = assignments.reduce((acc, assignment) => {
        return assignment.employeeId === uniqueId ? acc + assignment.dailyTimeInput : acc;
      // tslint:disable-next-line: align
      }, 0);
      const updatedAssignment: AddEmployeeToProjectDTO = {
        employeeId: uniqueId,
        dailyTimeInput: employeeAssignmentTime,
        skillId: null,
      };

      return updatedAssignment;
    });
    updatedAssignments.forEach(async (assignment) => {
      const employee: Employee = await this.employeeRepository.findOne(assignment.employeeId);

      const adjustedWorkingHours: number = employee.workingHoursLeft - assignment.dailyTimeInput;

      await this.employeeRepository.update(employee.id, { workingHoursLeft: adjustedWorkingHours });
    });
  }

  private async restoreEmployeesWorkingHours(projectEmployees: ProjectEmployee[]): Promise<void> {
    const employees: Employee[] = await Promise.all(projectEmployees.map(projectEmployee =>
      projectEmployee.employee));

    const employeeIds: number[] = employees.map(employee => employee.id);
    const uniqueEmployeeIds: number[] = employeeIds.filter((id, i) =>
    employeeIds.indexOf(id) === i);

    await Promise.all(uniqueEmployeeIds.map(async (employeeId) => {
      const employee: Employee = employees.find(employee => employee.id === employeeId);

      const allEmployeeAssignments: ProjectEmployee[] = await this.projectEmployeeRepo.find(
        { where: { employee, isDeleted: false } });
      const employeeProjectAssignments: ProjectEmployee[] = allEmployeeAssignments.filter(
        employeeAssignment => projectEmployees.some(
          projectEmployee => projectEmployee.id === employeeAssignment.id));

      const employeeProjectAssignmentsTime: number = employeeProjectAssignments.reduce(
        (acc, assignment) => { return acc + assignment.dailyTimeInput; }, 0);

      const adjustedWorkingHours: number = employee.workingHoursLeft
      + employeeProjectAssignmentsTime;

      await this.employeeRepository.update(employee.id, {
        workingHoursLeft: adjustedWorkingHours });
    }));
    projectEmployees.forEach(async projectEmployee => await this.projectEmployeeRepo.update(
        projectEmployee.id, { isDeleted: true }));
  }

  private validateProjectAssignments(
    projectSkills: AddSkillToProjectDTO[],
    projectEmployees: AddEmployeeToProjectDTO[]): void {
    const skillIds: number[] = projectSkills.map(skill => skill.skillId);

    if (skillIds.some((id, i) => skillIds.indexOf(id) !== i)) {
      throw new ProjectPlanningSystemError('Duplicate entries for skills not allowed!', 400);
    }
    let duplicateEmployees = false;

    skillIds.forEach((skillId: number) => {
      const employees: AddEmployeeToProjectDTO[] = projectEmployees.filter(
          employee => employee.skillId === skillId);
      const employeeIds: number[] = employees.map(employee => employee.employeeId);
      if (employeeIds.some((id, i) => employeeIds.indexOf(id) !== i)) {
        duplicateEmployees = true;
      }
    });
    if (duplicateEmployees) {
      throw new ProjectPlanningSystemError('Duplicate entries for employees not allowed!', 400);
    }
    const employeeIds: number[] = projectEmployees.map(
      projectEmployee => projectEmployee.employeeId);

    const uniqueEmployeeIds: number[] = employeeIds.filter((id, i) =>
      employeeIds.indexOf(id) === i);

    uniqueEmployeeIds.forEach((uniqueEmployeeId) => {
      const employeeDailyHoursOnProject: number = projectEmployees.reduce(
        (acc, projectEmployee) => {
          return projectEmployee.employeeId === uniqueEmployeeId ?
        acc + projectEmployee.dailyTimeInput : acc;
      // tslint:disable-next-line: align
        }, 0);
      if (employeeDailyHoursOnProject > 8) {
        throw new ProjectPlanningSystemError(
          'Employee daily working time cannot exceed 8 hours!', 400);
      }
    });
  }
}
