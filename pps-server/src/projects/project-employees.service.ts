import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { ProjectEmployee } from '../database/entities/projectEmployee.entity';
import { Employee } from '../database/entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProjectsService } from './projects.service';
import { ReturnProjectEmployeeDTO } from './models/return-project-employee.dto';
import { Project } from '../database/entities/project.entity';
import { plainToClass } from 'class-transformer';
import { AddEmployeeToProjectDTO } from './models/add-employee.dto';
import { Skill } from '../database/entities/skill.entity';
import { ProjectPlanningSystemError } from '../common/exceptions/pps.error';
import { UpdateProjectEmployeeTimeDTO } from './models/update-employee.dto';
import { RemoveProjectEmployeeDTO } from './models/remove-employee.dto';
import { ProjectReturnDTO } from './models/project-return.dto';
import { ProjectStatus } from './enums/project-status.enum';
import { ProjectSkill } from '../database/entities/projectSkill.entity';
import { ReturnUserDTO } from '../users/models/user-return.dto';

@Injectable()
export class ProjectEmployeesService {

  public constructor(
        @InjectRepository(ProjectEmployee)
         private readonly projectEmployeeRepo: Repository<ProjectEmployee>,
        @InjectRepository(ProjectSkill) private readonly projectSkillRepo: Repository<ProjectSkill>,
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(Skill) private readonly skillRepository: Repository<Skill>,
        @Inject(forwardRef(() => ProjectsService))
        private readonly projectsService: ProjectsService,
    ) {}

  public async getAllProjectEmployees(projectId: number): Promise<ReturnProjectEmployeeDTO[]> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    const projectEmployees: ProjectEmployee[] = (await project.projectEmployees).filter(
      projectEmployee => !projectEmployee.isDeleted);

    return plainToClass(ReturnProjectEmployeeDTO, projectEmployees, {
      excludeExtraneousValues: true });
  }

  public async addEmployeeToProject(
      projectId: number, employeeAssignment: AddEmployeeToProjectDTO,
      user: ReturnUserDTO): Promise<ReturnProjectEmployeeDTO> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    const skill: Skill = await this.skillRepository.findOne(
        employeeAssignment.skillId, { where: { isDeleted: false } });

    const employee: Employee = await this.employeeRepository.findOne(
        employeeAssignment.employeeId, { where: { isDeleted: false } });

    let existingEmployee: Employee;

    if (!project.isNew) {
      if (project.status === ProjectStatus.CLOSED) {
        throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
      }
      if ((await project.manager).id !== user.id) {
        throw new ProjectPlanningSystemError(`You are not authorized to add new employees to project with ID: ${project.id}!`, 401);
      }
      await this.validateEmployeeProjectAssignment(project, employeeAssignment);

      const existingProjectSkill: ProjectEmployee[] = (await project.projectEmployees).filter(
        projectEmployee => projectEmployee.skillName === skill.name && !projectEmployee.isDeleted);

      const existingProjectSkillEmployees: Employee[] = await Promise.all(existingProjectSkill.map(
        async existingSkill => await existingSkill.employee));

      existingEmployee = existingProjectSkillEmployees.find(
        existingProjectSkillEmployee => existingProjectSkillEmployee.id === employee.id);
    }
    if (existingEmployee) {
      throw new ProjectPlanningSystemError(`Employee with ID: ${employee.id} already works with ${skill.name} on project with ID: ${projectId}!`, 400);
    }
    if (!project.isNew) {
      const adjustedWorkingHours: number = employee.workingHoursLeft
    - employeeAssignment.dailyTimeInput;

      await this.employeeRepository.update(employee.id, { workingHoursLeft: adjustedWorkingHours });
    }
    const newEmployeeAssignment: ProjectEmployee =
     this.projectEmployeeRepo.create(employeeAssignment);
    newEmployeeAssignment.employeeName = `${employee.firstName} ${employee.lastName}`;
    newEmployeeAssignment.skillName = skill.name;
    newEmployeeAssignment.projectName = project.name;
    newEmployeeAssignment.projectId = projectId;
    newEmployeeAssignment.employee = Promise.resolve(employee);
    newEmployeeAssignment.skill = Promise.resolve(skill);
    newEmployeeAssignment.project = Promise.resolve(project);

    await this.projectEmployeeRepo.save(newEmployeeAssignment);

    if (!project.isNew && (project.managementDailyHours > 0 ||
      (project.managementDailyHours === 0 && project.managementTotalHours === 0)
      || project.managementHoursPassed >= project.managementTotalHours)) {
      this.projectsService.calculateProjectRemainingDays(project.id);
    }

    return plainToClass(ReturnProjectEmployeeDTO, newEmployeeAssignment, {
      excludeExtraneousValues: true });
  }

  public async updateProjectEmployeeTime(
    projectId: number, update: UpdateProjectEmployeeTimeDTO,
    user: ReturnUserDTO): Promise<ReturnProjectEmployeeDTO> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    if (project.status === ProjectStatus.CLOSED) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
    }
    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to edit employee times on project with ID: ${project.id}!`, 401);
    }
    const projectEmployee: ProjectEmployee = (await project.projectEmployees).find(
      projectEmployee => projectEmployee.id === update.projectEmployeeId
      && !projectEmployee.isDeleted);

    if (projectEmployee === undefined) {
      throw new ProjectPlanningSystemError(`Employee assignment with ID: ${update.projectEmployeeId} does not exist on project with ID: ${projectId}!`, 404);
    }
    const projectEmployeeSkill: ProjectSkill = (await project.projectSkills).find(projectSkill =>
      projectSkill.skillName === projectEmployee.skillName && !projectSkill.isDeleted);

    if (update.dailyTimeInput > 0 &&
      projectEmployeeSkill.hoursWorked >= projectEmployeeSkill.totalHours) {
      throw new ProjectPlanningSystemError(`The ${projectEmployeeSkill.skillName} activity on project with ID: ${project.id} has been finished!`, 400);
    }
    const employee: Employee = await projectEmployee.employee;
    employee.workingHoursLeft += projectEmployee.dailyTimeInput;

    if (employee.workingHoursLeft < update.dailyTimeInput) {
      throw new ProjectPlanningSystemError(`Employee with ID: ${employee.id} does not have the required working hours!`, 400);
    }
    employee.workingHoursLeft -= update.dailyTimeInput;

    await this.employeeRepository.save(employee);
    await this.projectEmployeeRepo.update(projectEmployee.id, {
      dailyTimeInput: update.dailyTimeInput });

    this.projectsService.calculateProjectRemainingDays(project.id);

    return plainToClass(ReturnProjectEmployeeDTO, projectEmployee, {
      excludeExtraneousValues: true });
  }

  public async removeEmployeeFromProject(
    projectId: number, projectEmployeeId: RemoveProjectEmployeeDTO,
    user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    if (project.status === ProjectStatus.CLOSED) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
    }
    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to remove employees from project with ID: ${project.id}!`, 401);
    }
    const projectEmployee: ProjectEmployee = (await project.projectEmployees).find(
        projectEmployee => projectEmployee.id === projectEmployeeId.projectEmployeeId
        && !projectEmployee.isDeleted);

    if (projectEmployee === undefined) {
      throw new ProjectPlanningSystemError(`Employee assignment with ID: ${projectEmployeeId.projectEmployeeId} does not exist on project with ID: ${projectId}!`, 404);
    }
    const employee: Employee = await projectEmployee.employee;

    const adjustedWorkingHours: number = employee.workingHoursLeft
    + projectEmployee.dailyTimeInput;

    await this.employeeRepository.update(employee.id, { workingHoursLeft: adjustedWorkingHours });
    await this.projectEmployeeRepo.update(projectEmployee.id, { isDeleted: true });

    this.projectsService.calculateProjectRemainingDays(project.id);

    return plainToClass(ProjectReturnDTO, project, { excludeExtraneousValues: true });
  }

  public async validateEmployeeProjectAssignment(
    project: Project, employeeAssignment: AddEmployeeToProjectDTO): Promise<void> {
    const skill: Skill = await this.skillRepository.findOne(
        employeeAssignment.skillId, { where: { isDeleted: false } });

    if (skill === undefined) {
      throw new ProjectPlanningSystemError(`Skill with ID: ${employeeAssignment.skillId} does not exist!`, 404);
    }
    const projectSkills: ProjectSkill[] = await this.projectSkillRepo.find(
      { where: { project, isDeleted: false } });

    const requiredProjectSkills: ProjectSkill[] = projectSkills.filter(projectSkill =>
        projectSkill.totalHours > 0 && projectSkill.hoursWorked < projectSkill.totalHours
        && projectSkill.skillName === skill.name);

    if (!requiredProjectSkills.length) {
      throw new ProjectPlanningSystemError(`Skill: ${skill.name} is not currently required on project with ID: ${project.id}`, 400);
    }
    const employee: Employee = await this.employeeRepository.findOne(
        employeeAssignment.employeeId, { where: { isDeleted: false } });

    if (employee === undefined) {
      throw new ProjectPlanningSystemError(`Employee with ID: ${employeeAssignment.employeeId} does not exist!`, 404);
    }
    if (!(await employee.skills).find(
      employeeSkill => employeeSkill.id === skill.id && !employeeSkill.isDeleted)) {
      throw new ProjectPlanningSystemError(`Employee with ID: ${employee.id} does not have skill: ${skill.name}!`, 404);
    }
    if (employee.workingHoursLeft < employeeAssignment.dailyTimeInput) {
      throw new ProjectPlanningSystemError(`Employee with ID: ${employee.id} does not have the required working hours!`, 400);
    }
  }
}
