import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectSkill } from '../database/entities/projectSkill.entity';
import { Repository } from 'typeorm';
import { Skill } from '../database/entities/skill.entity';
import { ReturnProjectSkillDTO } from './models/return-project-skill.dto';
import { Project } from 'src/database/entities/project.entity';
import { plainToClass } from 'class-transformer';
import { AddSkillToProjectDTO } from './models/add-skill.dto';
import { ProjectPlanningSystemError } from 'src/common/exceptions/pps.error';
import { UpdateProjectSkillTimeDTO } from './models/update-skill.dto';
import { RemoveProjectSkillDTO } from './models/remove-skill.dto';
import { ProjectReturnDTO } from './models/project-return.dto';
import { ProjectsService } from './projects.service';
import { ProjectStatus } from './enums/project-status.enum';
import { ProjectEmployee } from '../database/entities/projectEmployee.entity';
import { ProjectEmployeesService } from './project-employees.service';
import { ReturnUserDTO } from '../users/models/user-return.dto';
import { Employee } from '../database/entities/employee.entity';

@Injectable()
export class ProjectSkillsService {

  public constructor(
    @InjectRepository(ProjectSkill) private readonly projectSkillRepo: Repository<ProjectSkill>,
    @InjectRepository(ProjectEmployee)
    private readonly projectEmployeeRepo: Repository<ProjectEmployee>,
    @InjectRepository(Skill) private readonly skillRepository: Repository<Skill>,
    @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
    @Inject(forwardRef(() => ProjectsService)) private readonly projectsService: ProjectsService,
    private readonly projectEmployeesService: ProjectEmployeesService,
    ) {}

  public async getAllProjectSkills(projectId: number): Promise<ReturnProjectSkillDTO[]> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    const projectSkills: ProjectSkill[] = (await project.projectSkills).filter(
      projectSkill => !projectSkill.isDeleted);

    return plainToClass(ReturnProjectSkillDTO, projectSkills, { excludeExtraneousValues: true });
  }

  public async addSkillToProject(
        projectId: number, skillAssignment: AddSkillToProjectDTO,
        user: ReturnUserDTO): Promise<ReturnProjectSkillDTO> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    const skill: Skill = await this.skillRepository.findOne(
          skillAssignment.skillId, { where: { isDeleted: false } });

    if (skill === undefined) {
      throw new ProjectPlanningSystemError(`Skill with ID: ${skillAssignment.skillId} does not exist!`, 404);
    }
    let existingProjectSkill: ProjectSkill;

    if (!project.isNew) {
      if (project.status === ProjectStatus.CLOSED) {
        throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
      }
      if ((await project.manager).id !== user.id) {
        throw new ProjectPlanningSystemError(`You are not authorized to add new skills to project with ID: ${project.id}!`, 401);
      }
      existingProjectSkill = (await project.projectSkills).find(
        projectSkill => projectSkill.skillName === skill.name && !projectSkill.isDeleted);
    }
    if (existingProjectSkill) {
      throw new ProjectPlanningSystemError(
        `Skill: ${skill.name} already added to project with ID: ${projectId}!`, 400);
    }
    const newSkillAssignment: ProjectSkill = this.projectSkillRepo.create(skillAssignment);
    newSkillAssignment.skillName = skill.name;
    newSkillAssignment.project = Promise.resolve(project);
    newSkillAssignment.skill = Promise.resolve(skill);

    await this.projectSkillRepo.save(newSkillAssignment);

    if (project.remainingDays > 0 && !project.isNew) {
      await this.projectsService.calculateProjectRemainingDays(project.id);
    }

    return plainToClass(ReturnProjectSkillDTO, newSkillAssignment, {
      excludeExtraneousValues: true });
  }

  public async updateProjectSkillTime(
        projectId: number, update: UpdateProjectSkillTimeDTO,
        user: ReturnUserDTO): Promise<ReturnProjectSkillDTO> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    if (project.status === ProjectStatus.CLOSED) {
      throw new ProjectPlanningSystemError(`Project with ID: ${project.id} is closed!`, 400);
    }
    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to edit skill times on project with ID: ${project.id}!`, 401);
    }
    const projectSkill: ProjectSkill = (await project.projectSkills).find(
          projectSkill => projectSkill.id === update.projectSkillId && !projectSkill.isDeleted);

    if (projectSkill === undefined) {
      throw new ProjectPlanningSystemError(`Skill assignment with ID: ${update.projectSkillId} does not exist on project with ID: ${projectId}!`, 404);
    }
    if (update.totalHours < projectSkill.hoursWorked) {
      throw new ProjectPlanningSystemError(`New ${projectSkill.skillName} total hours already passed!`, 400);
    }

    await this.projectSkillRepo.update(projectSkill.id, { totalHours: update.totalHours });

    this.projectsService.calculateProjectRemainingDays(project.id);

    return plainToClass(ReturnProjectSkillDTO, projectSkill, { excludeExtraneousValues: true });
  }

  public async removeSkillFromProject(
        projectId: number, skill: RemoveProjectSkillDTO,
        user: ReturnUserDTO): Promise<ProjectReturnDTO> {
    const project: Project = await this.projectsService.findProjectById(projectId);

    if ((await project.manager).id !== user.id) {
      throw new ProjectPlanningSystemError(`You are not authorized to remove skills from project with ID: ${project.id}!`, 401);
    }
    const projectSkill: ProjectSkill = (await project.projectSkills).find(
          projectSkill => projectSkill.id === skill.projectSkillId && !projectSkill.isDeleted);

    if (projectSkill === undefined) {
      throw new ProjectPlanningSystemError(`Skill assignment with ID: ${skill.projectSkillId} does not exist on project with ID: ${projectId}!`, 404);
    }
    const projectSkillEmployees: ProjectEmployee[] = (await project.projectEmployees).filter(
      projectEmployee => projectEmployee.skillName === projectSkill.skillName &&
      !projectEmployee.isDeleted);

    if (projectSkillEmployees.length) {
      projectSkillEmployees.forEach(async (projectSkillEmployee) => {
        const employee: Employee = await projectSkillEmployee.employee;

        const adjustedWorkingHours: number = employee.workingHoursLeft
        + projectSkillEmployee.dailyTimeInput;

        await this.employeeRepository.update(employee.id, {
          workingHoursLeft: adjustedWorkingHours });

        await this.projectEmployeeRepo.update(projectSkillEmployee.id, { isDeleted: true });
      });
    }
    await this.projectSkillRepo.update(projectSkill.id, { isDeleted: true });

    this.projectsService.calculateProjectRemainingDays(project.id);

    return plainToClass(ProjectReturnDTO, project, { excludeExtraneousValues: true });
  }
}
