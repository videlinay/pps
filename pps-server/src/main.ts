import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ProjectPlanningSystemErrorFilter } from './common/filters/pps-error.filter';
import { ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.use(helmet());

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  app.useGlobalFilters(new ProjectPlanningSystemErrorFilter());

  await app.listen(+app.get(ConfigService).get('PORT'));
}
bootstrap();
