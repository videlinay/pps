import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from '../database/entities/employee.entity';
import { Repository } from 'typeorm';
import { QueryEmployeeDTO } from './models/query-employee.dto';
import { ReturnEmployeeDTO } from './models/return-employee.dto';
import { plainToClass } from 'class-transformer';
import { ProjectPlanningSystemError } from '../common/exceptions/pps.error';
import { CreateEmployeeDTO } from './models/create-employee.dto';
import { User } from '../database/entities/user.entity';
import { Skill } from '../database/entities/skill.entity';
import { AddNewSkillDTO } from './models/add-new-skill.dto';
import { ReturnSkillDTO } from '../skills/models/return-skill.dto';
import { ChangeEmployeeManagerDTO } from './models/change-manager.dto';
import { EmployeeCurrentProjectDTO } from './models/employee-current-project.dto';
import { ProjectEmployee } from '../database/entities/projectEmployee.entity';

@Injectable()
export class EmployeesService {

  public constructor(
        @InjectRepository(Employee) private readonly employeeRepository: Repository<Employee>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Skill) private readonly skillRepository: Repository<Skill>,
    ) { }

  public async getEmployees(query: QueryEmployeeDTO): Promise<ReturnEmployeeDTO[]> {
    let employees: Employee[] = await this.employeeRepository.find({
      where: { isDeleted: false }, order: { firstName: 'ASC' },
    });

    if (query.firstName) {
      employees = employees.filter(employee =>
         employee.firstName.toLowerCase().includes(query.firstName.toLowerCase()));
    }
    if (query.lastName) {
      employees = employees.filter(employee =>
         employee.lastName.toLowerCase().includes(query.lastName.toLowerCase()));
    }
    if (query.position) {
      employees = employees.filter(employee =>
         employee.position.toLowerCase().includes(query.position.toLowerCase()));
    }

    return plainToClass(ReturnEmployeeDTO, employees, { excludeExtraneousValues: true });
  }

  public async getEmployeeById(employeeId: number): Promise<ReturnEmployeeDTO> {
    const employee: Employee = await this.findEmployeeById(employeeId);

    return plainToClass(ReturnEmployeeDTO, employee, { excludeExtraneousValues: true });
  }

  public async createEmployee(employee: CreateEmployeeDTO): Promise<ReturnEmployeeDTO> {
    const directManager: User = await this.userRepository.findOne(
      employee.managerId, { where: { isDeleted: false } });

    if (directManager === undefined) {
      throw new ProjectPlanningSystemError(
        `Manager with ID: ${employee.managerId} does not exist!`, 404);
    }
    const skills: Skill[] = await Promise.all(employee.skillIds.map(async id =>
      await this.skillRepository.findOne(id, { where: { isDeleted: false } })));

    if (skills.includes(undefined)) {
      throw new ProjectPlanningSystemError('Skill not found', 404);
    }
    const newEmployee: Employee = this.employeeRepository.create(employee);
    newEmployee.directManager = `${directManager.firstName} ${directManager.lastName}`;
    newEmployee.manager = Promise.resolve(directManager);
    newEmployee.skills = Promise.resolve(skills);
    newEmployee.projectEmployees = Promise.resolve([]);

    await this.employeeRepository.save(newEmployee);

    return plainToClass(ReturnEmployeeDTO, newEmployee, { excludeExtraneousValues: true });
  }

  public async deleteEmployee(employeeId: number): Promise<void> {
    const employee: Employee = await this.findEmployeeById(employeeId);

    await this.employeeRepository.update(employee.id, { isDeleted: true });
  }

  public async addNewSkillToEmployee(
    employeeId: number, skill: AddNewSkillDTO): Promise<ReturnEmployeeDTO> {
    const employee: Employee = await this.findEmployeeById(employeeId);

    const newSkill: Skill = await this.skillRepository.findOne(
      skill.skillId, { where: { isDeleted: false } });

    if (newSkill === undefined) {
      throw new ProjectPlanningSystemError(`Skill with ID: ${skill.skillId} does not exist!`, 404);
    }
    const employeeCurrentSkills: Skill[] = await employee.skills;

    if (employeeCurrentSkills.find(skill => skill.id === newSkill.id)) {
      throw new ProjectPlanningSystemError(`${employee.lastName} already has ${newSkill.name} as a skill!`, 400);
    }
    employeeCurrentSkills.push(newSkill);

    employee.skills = Promise.resolve(employeeCurrentSkills);

    await this.employeeRepository.save(employee);

    return plainToClass(ReturnEmployeeDTO, employee, { excludeExtraneousValues: true });
  }

  public async changeEmployeeManager(
    employeeId: number, manager: ChangeEmployeeManagerDTO): Promise<ReturnEmployeeDTO> {
    const employee: Employee = await this.findEmployeeById(employeeId);

    const newManager: User = await this.userRepository.findOne(
      manager.managerId, { where: { isDeleted: false } });

    if (newManager === undefined) {
      throw new ProjectPlanningSystemError(
          `Manager with ID: ${manager.managerId} does not exist!`, 404);
    }
    employee.manager = Promise.resolve(newManager);
    employee.directManager = `${newManager.firstName} ${newManager.lastName}`;

    await this.employeeRepository.save(employee);

    return plainToClass(ReturnEmployeeDTO, employee, { excludeExtraneousValues: true });
  }

  public async getEmployeeSkills(employeeId: number): Promise<ReturnSkillDTO[]> {
    const employee: Employee = await this.findEmployeeById(employeeId);

    const skills: Skill[] = (await employee.skills).filter(skill => !skill.isDeleted);

    return plainToClass(ReturnSkillDTO, skills, { excludeExtraneousValues: true });
  }

  public async getEmployeeProjects(employeeId: number): Promise<EmployeeCurrentProjectDTO[]> {
    const employee: Employee = await this.findEmployeeById(employeeId);

    const currentProjects: ProjectEmployee[] = (await employee.projectEmployees).filter(
      projectEmployee => !projectEmployee.isDeleted);

    return plainToClass(EmployeeCurrentProjectDTO, currentProjects, {
      excludeExtraneousValues: true });
  }

  private async findEmployeeById(employeeId: number): Promise<Employee> {
    const employee: Employee = await this.employeeRepository.findOne(
      employeeId, { where: { isDeleted: false } });

    if (employee === undefined) {
      throw new ProjectPlanningSystemError(`Employee with ID: ${employeeId} does not exist!`, 404);
    }

    return employee;
  }
}
