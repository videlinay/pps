import { Expose } from 'class-transformer';

export class EmployeeCurrentProjectDTO {
  @Expose()
    public projectName: string;

  @Expose()
    public projectId: number;

  @Expose()
    public skillName: string;

  @Expose()
    public dailyTimeInput: number;
}
