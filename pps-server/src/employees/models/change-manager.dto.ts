import { Min } from 'class-validator';

export class ChangeEmployeeManagerDTO {
  @Min(1)
  public managerId: number;
}
