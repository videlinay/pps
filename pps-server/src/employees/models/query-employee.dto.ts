import { IsOptional } from 'class-validator';

export class QueryEmployeeDTO {
  @IsOptional()
    public firstName?: string;

  @IsOptional()
    public lastName?: string;

  @IsOptional()
    public position?: string;
}
