import { Expose } from 'class-transformer';

export class ReturnUserDTO {
  @Expose()
  public id: number;

  @Expose()
  public firstName: string;

  @Expose()
  public lastName: string;

  @Expose()
  public email: string;

  @Expose()
  public position: string;

  @Expose()
  public directManagerName: string;

  @Expose()
  public role: string;

  @Expose()
  public workingHoursLeft: number;
}
