import { Length, Matches, IsEmail, IsOptional } from 'class-validator';

export class UpdateUserDTO {
  @IsOptional()
  @IsEmail()
  public email?: string;

  @IsOptional()
  @Length(4, 20)
  @Matches(/^[a-z0-9]+$/i, {
    message: 'Password must contain only numbers and letters!',
  })
  public password?: string;
}
