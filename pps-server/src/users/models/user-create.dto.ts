import { Length, Matches, IsEmail, IsBoolean, Allow } from 'class-validator';

export class CreateUserDTO {
  @Length(2, 10)
  public firstName: string;

  @Length(2, 20)
  public lastName: string;

  @Length(2, 40)
  public position: string;

  @Allow()
  public directManagerId: number;

  @IsBoolean()
  public isAdmin: boolean;

  @IsEmail()
  public email: string;

  @Length(4, 20)
  @Matches(/^[a-z0-9]+$/i, {
    message: 'Password must contain only numbers and letters!',
  })
  public password: string;
}
