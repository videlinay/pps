// tslint:disable-next-line: max-line-length
import { Controller, Post, Delete, Body, Param, ParseIntPipe, Put, UseGuards } from '@nestjs/common';
import { SkillsService } from '../skills/skills.service';
import { CreateSkillDTO } from '../skills/models/create-skill.dto';
import { ReturnSkillDTO } from '../skills/models/return-skill.dto';
import { EmployeesService } from '../employees/employees.service';
import { CreateEmployeeDTO } from '../employees/models/create-employee.dto';
import { ReturnEmployeeDTO } from '../employees/models/return-employee.dto';
import { AddNewSkillDTO } from '../employees/models/add-new-skill.dto';
import { ChangeEmployeeManagerDTO } from '../employees/models/change-manager.dto';
import { UsersService } from './users.service';
import { CreateUserDTO } from './models/user-create.dto';
import { ReturnUserDTO } from './models/user-return.dto';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { ChangeUserManagerDTO } from './models/change-user-manager.dto';

@Controller('admin')
@UseGuards(AuthenticationGuard, AdminGuard)
export class AdminController {

  public constructor(
        private readonly skillsService: SkillsService,
        private readonly employeesService: EmployeesService,
        private readonly usersService: UsersService,
    ) {}

  @Post('skills')
    public async createSkill(@Body() body: CreateSkillDTO): Promise<ReturnSkillDTO> {
    return await this.skillsService.createSkill(body);
  }

  @Delete('skills/:skillId')
    public async deleteSkill(@Param('skillId', ParseIntPipe) id: number): Promise<void> {
    return await this.skillsService.deleteSkill(id);
  }

  @Post('employees')
  public async createEmployee(@Body() body: CreateEmployeeDTO): Promise<ReturnEmployeeDTO> {
    return await this.employeesService.createEmployee(body);
  }

  @Post('employees/:employeeId')
  public async addNewSkillToEmployee(
      @Param('employeeId', ParseIntPipe) id: number,
      @Body() body: AddNewSkillDTO): Promise<ReturnEmployeeDTO> {
    return await this.employeesService.addNewSkillToEmployee(id, body);
  }

  @Put('employees/:employeeId')
  public async changeEmployeeManager(
    @Param('employeeId', ParseIntPipe) id: number,
    @Body() body: ChangeEmployeeManagerDTO): Promise<ReturnEmployeeDTO> {
    return await this.employeesService.changeEmployeeManager(id, body);
  }

  @Delete('employees/:employeeId')
  public async deleteEmployee(@Param('employeeId', ParseIntPipe) id: number): Promise<void> {
    return await this.employeesService.deleteEmployee(id);
  }

  @Post('users')
  public async createUser(@Body() body: CreateUserDTO): Promise<ReturnUserDTO> {
    return await this.usersService.createUser(body);
  }

  @Put('users/:userId')
  public async changeUserManager(
    @Param('userId', ParseIntPipe) id: number,
    @Body() body: ChangeUserManagerDTO): Promise<ReturnUserDTO> {
    return await this.usersService.changeUserManager(id, body);
  }

  @Delete('users/:userId')
  public async deleteUser(@Param('userId', ParseIntPipe) id: number): Promise<void> {
    return await this.usersService.deleteUser(id);
  }
}
