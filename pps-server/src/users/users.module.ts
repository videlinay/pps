import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User } from '../database/entities/user.entity';
import { AdminController } from './admin.controller';
import { SkillsService } from '../skills/skills.service';
import { Skill } from '../database/entities/skill.entity';
import { EmployeesService } from '../employees/employees.service';
import { Employee } from '../database/entities/employee.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Skill, Employee]),
  ],
  controllers: [UsersController, AdminController],
  providers: [UsersService, SkillsService, EmployeesService],
  exports: [UsersService],
})

export class UsersModule { }
