import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Skill } from '../database/entities/skill.entity';
import { Repository } from 'typeorm';
import { ReturnSkillDTO } from './models/return-skill.dto';
import { plainToClass } from 'class-transformer';
import { ProjectPlanningSystemError } from '../common/exceptions/pps.error';
import { CreateSkillDTO } from './models/create-skill.dto';
import { ReturnEmployeeDTO } from '../employees/models/return-employee.dto';
import { Employee } from '../database/entities/employee.entity';

@Injectable()
export class SkillsService {

  public constructor (
      @InjectRepository(Skill) private readonly skillRepository: Repository<Skill>,
      ) {}

  public async getAllSkills(): Promise<ReturnSkillDTO[]> {
    const skills: Skill[] = await this.skillRepository.find({
      where: { isDeleted: false }, order: { name: 'ASC' },
    });

    return plainToClass(ReturnSkillDTO, skills, { excludeExtraneousValues: true });
  }

  public async getSkillById(skillId: number): Promise<ReturnSkillDTO> {
    const skill: Skill = await this.findSkillById(skillId);

    return plainToClass(ReturnSkillDTO, skill, { excludeExtraneousValues: true });
  }

  public async createSkill(skill: CreateSkillDTO): Promise<ReturnSkillDTO> {
    const currentSkills: ReturnSkillDTO[] = await this.getAllSkills();

    if (currentSkills.some(currentSkill =>
      currentSkill.name.toLowerCase().trim() === skill.name.toLowerCase().trim())) {
      throw new ProjectPlanningSystemError('This skill already exists!', 400);
    }
    const newSkill: Skill = this.skillRepository.create(skill);
    newSkill.employees = Promise.resolve([]);
    newSkill.projectSkills = Promise.resolve([]);

    await this.skillRepository.save(newSkill);

    return plainToClass(ReturnSkillDTO, newSkill, { excludeExtraneousValues: true });
  }

  public async deleteSkill(skillId: number): Promise<void> {
    const skill: Skill = await this.findSkillById(skillId);

    await this.skillRepository.update(skill.id, { isDeleted: true });
  }

  public async getSkillEmployees(skillId: number): Promise<ReturnEmployeeDTO[]> {
    const skill: Skill = await this.findSkillById(skillId);

    const employees: Employee[] = (await skill.employees).filter(employee => !employee.isDeleted);

    return plainToClass(ReturnEmployeeDTO, employees, { excludeExtraneousValues: true });
  }

  private async findSkillById(skillId: number): Promise<Skill> {
    const skill: Skill = await this.skillRepository.findOne(
      skillId, { where: { isDeleted: false } });

    if (skill === undefined) {
      throw new ProjectPlanningSystemError(`Skill with ID: ${skillId} does not exist!`, 404);
    }

    return skill;
  }
}
