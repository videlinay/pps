import { createParamDecorator, ExecutionContext } from '@nestjs/common';

// tslint:disable-next-line: variable-name
export const User = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    return request.user;
  },
);
