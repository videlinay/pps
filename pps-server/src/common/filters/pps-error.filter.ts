import { Catch, ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { ProjectPlanningSystemError } from '../exceptions/pps.error';
import { Response } from 'express';

@Catch(ProjectPlanningSystemError)
export class ProjectPlanningSystemErrorFilter implements ExceptionFilter {
  public catch(exception: ProjectPlanningSystemError, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
